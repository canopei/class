package class

import (
	classPb "bitbucket.org/canopei/class/protobuf"
)

type ESClass struct {
	Name          string         `json:"className"`
	Description   string         `json:"classDescription"`
	SessionType   *ESSessionType `json:"sessionType"`
	LevelID       int32          `json:"levelId"`
	Active        bool           `json:"active"`
	IsAvailable   bool           `json:"isAvailable"`
	CityNA        string         `json:"cityNA"`
	StartDateTime string         `json:"startDatetime"`
	EndDateTime   string         `json:"endDatetime"`
	Staff         *ESStaff       `json:"staff"`
	Location      *ESLocation    `json:"location"`
}

type ESSessionType struct {
	ID      int32      `json:"id"`
	Name    string     `json:"sessionTypeName"`
	Program *ESProgram `json:"program"`
}

type ESProgram struct {
	ID           int32  `json:"programId"`
	Name         string `json:"programName"`
	ScheduleType int32  `json:"scheduleType"`
}

type ESStaff struct {
	UUID     string `json:"uuid"`
	Featured int32  `json:"featured"`
}

type ESLocation struct {
	UUID string `json:"uuid"`
}

// GetESClass creates a ESClass from a Site object
func GetESClass(dbClass *classPb.Class) *ESClass {
	esStaff := &ESStaff{}
	if dbClass.Staff != nil {
		esStaff = &ESStaff{
			UUID:     dbClass.Staff.Uuid,
			Featured: dbClass.Staff.Featured,
		}
	}

	return &ESClass{
		Name:        dbClass.Description.Name,
		Description: dbClass.Description.Description,
		SessionType: &ESSessionType{
			ID:   dbClass.Description.SessionType.Id,
			Name: dbClass.Description.SessionType.Name,
			Program: &ESProgram{
				ID:           dbClass.Description.SessionType.Program.Id,
				Name:         dbClass.Description.SessionType.Program.Name,
				ScheduleType: int32(dbClass.Description.SessionType.Program.ScheduleType),
			},
		},
		Staff: esStaff,
		Location: &ESLocation{
			UUID: dbClass.Location.Uuid,
		},
		LevelID:       dbClass.Description.LevelId,
		Active:        dbClass.Active,
		IsAvailable:   dbClass.IsAvailable,
		CityNA:        dbClass.Location.CityGroupName,
		StartDateTime: dbClass.StartDatetime,
		EndDateTime:   dbClass.EndDatetime,
	}
}
