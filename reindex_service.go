package class

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
	"gopkg.in/olivere/elastic.v5"
)

// ReindexService is the implementation of a reindex service (from DB to ES)
type ReindexService struct {
	Logger               *logrus.Entry
	Queue                *nsq.Producer
	ElasticBulkProcessor *elastic.BulkProcessor
	ElasticsearchService *elasticsearch.Service
	ClassService         classPb.ClassServiceClient
	StaffService         staffPb.StaffServiceClient
	SiteService          sitePb.SiteServiceClient
}

// NewReindexService creates a new ReindexService instance
func NewReindexService(
	logger *logrus.Entry,
	queue *nsq.Producer,
	elasticBulkProcessor *elastic.BulkProcessor,
	classService classPb.ClassServiceClient,
	staffService staffPb.StaffServiceClient,
	siteService sitePb.SiteServiceClient,
) *ReindexService {
	return &ReindexService{
		Logger:               logger,
		Queue:                queue,
		ElasticBulkProcessor: elasticBulkProcessor,
		ClassService:         classService,
		StaffService:         staffService,
		SiteService:          siteService,
	}
}

// ReindexSiteClassSchedules syncs from DB to ES the class schedules
func (s *ReindexService) ReindexSiteClassSchedules(siteUUID string, indexName string) error {
	ctx := context.Background()

	_, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		s.Logger.Errorf("Cannot fetch site by UUIDs '%s': %v", siteUUID, err)
		return fmt.Errorf("Cannot fetch site by UUID '%s': %v", siteUUID, err)
	}

	// Fetch locations
	locationsResult, err := s.SiteService.GetSitesLocations(ctx, &sitePb.GetSitesLocationsRequest{Uuids: siteUUID})
	if err != nil {
		s.Logger.Errorf("Cannot fetch locations for site UUIDs '%s': %v", siteUUID, err)
		return fmt.Errorf("Cannot fetch locations for site UUID '%s': %v", siteUUID, err)
	}
	if len(locationsResult.Locations) == 0 {
		s.Logger.Infof("No locatons found for site '%s'. Skipping.", siteUUID)
	}
	dbLocations := locationsResult.Locations

	for _, dbLocation := range dbLocations {
		staffDbCache := map[int32]*staffPb.Staff{}

		from := time.Now().AddDate(0, -3, 0).Format(sqlDatetimeFormat)
		// We fetch only the next 3 months
		to := time.Now().AddDate(0, 0, 90).Format(sqlDatetimeFormat)
		for true {
			dbClasses, err := s.ClassService.GetClassesByLocationID(ctx, &classPb.GetClassesByLocationIDRequest{
				LocationId: dbLocation.Id,
				From:       from,
				To:         to,
			})
			if err != nil {
				s.Logger.Errorf("Cannot fetch the classes for location ID %d: %v", dbLocation.Id, err)
				return fmt.Errorf("Cannot fetch the classes for location ID %d: %v", dbLocation.Id, err)
			}
			if len(dbClasses.Classes) == 0 {
				break
			}

			s.Logger.Debugf("Got %d classes...", len(dbClasses.Classes))

			var dbClass *classPb.Class
			for _, dbClass = range dbClasses.Classes {
				dbStaff, ok := staffDbCache[dbClass.StaffId]
				if !ok {
					response, err := s.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{
						Ids: []int32{dbClass.StaffId},
					})
					if err != nil {
						s.Logger.Errorf("Error while fetching staff ID %d from DB: %v", dbClass.StaffId, err)
						return fmt.Errorf("Error while fetching staff ID %d from DB: %v", dbClass.StaffId, err)
					}
					if len(response.Staff) == 0 {
						staffDbCache[dbClass.StaffId] = nil

						s.Logger.Errorf("Staff ID %d not found.", dbClass.StaffId)
						continue
					}
					dbStaff = response.Staff[0]
					staffDbCache[dbClass.StaffId] = dbStaff
				}

				dbClass.Staff = dbStaff
				dbClass.Location = dbLocation

				r := elastic.NewBulkIndexRequest().
					Index(indexName).
					Type("class").
					Parent(dbLocation.Uuid).
					Id(dbClass.Uuid).
					Doc(GetESClass(dbClass))
				s.ElasticBulkProcessor.Add(r)
			}

			lastStartDatetime, _ := time.Parse(sqlDatetimeFormat, dbClass.StartDatetime)
			to = lastStartDatetime.Add(time.Second * -1).Format(sqlDatetimeFormat)
		}
	}

	s.ElasticBulkProcessor.Flush()

	// Add to the queue a finished message for this site (only for the staff component)
	messageData := queueHelper.NewReindexSiteClassSchedulesMessage(siteUUID, indexName, queueHelper.ReindexStatusDone)
	message, _ := json.Marshal(messageData)
	s.Queue.Publish("reindex", message)

	return nil
}
