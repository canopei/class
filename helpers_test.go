package class

import (
	"strings"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/mindbody"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	"github.com/stretchr/testify/assert"
)

// Just a simple sanity check
func TestHasClassDescriptionChanged(t *testing.T) {
	assert := assert.New(t)

	dbClassDesc := &classPb.ClassDescription{Name: "foo"}
	mbClassDesc := &classPb.ClassDescription{Name: "bar"}
	assert.True(hasClassDescriptionChanged(dbClassDesc, mbClassDesc))

	dbClassDesc = &classPb.ClassDescription{Name: "foo"}
	mbClassDesc = &classPb.ClassDescription{Name: "foo"}
	assert.False(hasClassDescriptionChanged(dbClassDesc, mbClassDesc))
}

// Just a simple sanity check
func TestHasClassScheduleChanged(t *testing.T) {
	assert := assert.New(t)

	dbClassSch := &classPb.ClassSchedule{DayTuesday: true}
	mbClassSch := &classPb.ClassSchedule{DayTuesday: false}
	assert.True(hasClassScheduleChanged(dbClassSch, mbClassSch))

	dbClassSch = &classPb.ClassSchedule{DayTuesday: true}
	mbClassSch = &classPb.ClassSchedule{DayTuesday: true}
	assert.False(hasClassScheduleChanged(dbClassSch, mbClassSch))
}

func TestMBClassDescriptionToPbClassDescription(t *testing.T) {
	// ?imageversion=\d+ should be removed from the URL.
	mbClassDesc := &classMb.ClassDescription{ID: &mindbody.CustomInt32{Int: 1}, ImageURL: "http://www.foo.com/image.jpg?imageversion=12345"}
	pbClassDesc := mbClassDescriptionToPbClassDescription(mbClassDesc)

	assert.False(t, strings.Contains(pbClassDesc.ImageUrl, "imageversion="))
}

// Just a simple sanity check
func TestHasClassLevelChanged(t *testing.T) {
	assert := assert.New(t)

	dbClassLevel := &classPb.ClassLevel{Name: "foo"}
	mbClassLevel := &classPb.ClassLevel{Name: "bar"}
	assert.True(hasClassLevelChanged(dbClassLevel, mbClassLevel))

	dbClassLevel = &classPb.ClassLevel{Name: "foo"}
	mbClassLevel = &classPb.ClassLevel{Name: "foo"}
	assert.False(hasClassLevelChanged(dbClassLevel, mbClassLevel))
}

func TestHasSessionTypeChanged(t *testing.T) {
	assert := assert.New(t)

	dbSessionType := &classPb.SessionType{Name: "foo"}
	mbSessionType := &classPb.SessionType{Name: "bar"}
	assert.True(hasSessionTypeChanged(dbSessionType, mbSessionType))

	dbSessionType = &classPb.SessionType{Name: "foo"}
	mbSessionType = &classPb.SessionType{Name: "foo"}
	assert.False(hasSessionTypeChanged(dbSessionType, mbSessionType))
}

func TestHasProgramChanged(t *testing.T) {
	assert := assert.New(t)

	dbProgram := &classPb.Program{Name: "foo"}
	mbProgram := &classPb.Program{Name: "bar"}
	assert.True(hasProgramChanged(dbProgram, mbProgram))

	dbProgram = &classPb.Program{Name: "foo"}
	mbProgram = &classPb.Program{Name: "foo"}
	assert.False(hasProgramChanged(dbProgram, mbProgram))
}

func TestHasClassChanged(t *testing.T) {
	assert := assert.New(t)

	dbClass := &classPb.Class{TotalBooked: 2}
	mbClass := &classPb.Class{TotalBooked: 4}
	assert.True(hasClassChanged(dbClass, mbClass))

	dbClass = &classPb.Class{TotalBooked: 2}
	mbClass = &classPb.Class{TotalBooked: 2}
	assert.False(hasClassChanged(dbClass, mbClass))
}

// Test the schedule type conversion
func TestMBProgramToPbProgram(t *testing.T) {
	assert := assert.New(t)

	schType := classMb.ScheduleTypeAppointment

	mbProgram := &classMb.Program{ID: &mindbody.CustomInt32{Int: 1}, ScheduleType: &schType, CancelOffset: &mindbody.CustomInt32{Int: 1}}
	pbProgram, err := mbProgramToPbProgram(mbProgram)

	assert.Nil(err)
	assert.Equal(classPb.ScheduleType_value[string(*mbProgram.ScheduleType)], int32(pbProgram.ScheduleType))
}
