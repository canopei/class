package class

import (
	"fmt"

	classPb "bitbucket.org/canopei/class/protobuf"
	"google.golang.org/grpc"
)

// NewClient returns a gRPC client for interacting with the class service.
func NewClient(addr string) (classPb.ClassServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return classPb.NewClassServiceClient(conn), conn.Close, nil
}
