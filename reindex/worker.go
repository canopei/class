package main

import (
	"time"

	"encoding/json"

	"bitbucket.org/canopei/class"
	"bitbucket.org/canopei/class/config"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/logging"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
	"gopkg.in/olivere/elastic.v5"
)

// Worker is a queue consumer worker implementation
type Worker struct {
	ID                   int
	Config               *config.SyncConfig
	Logger               *logrus.Entry
	Queue                *nsq.Producer
	ElasticBulkProcessor *elastic.BulkProcessor
	ReindexService       *class.ReindexService
	ClassService         classPb.ClassServiceClient
	SiteService          sitePb.SiteServiceClient
	StaffService         staffPb.StaffServiceClient

	Buffers map[string]chan ChanEntry
}

// ChanEntry represents a channel entry containing the original NSQ message and a reindex message
type ChanEntry struct {
	NSQMessage     *nsq.Message
	ReindexMessage *queueHelper.ReindexMessage
}

// NewWorker creates a new Worker instance
func NewWorker(
	id int,
	config *config.Config,
	logger *logrus.Entry,
	classService classPb.ClassServiceClient,
	siteService sitePb.SiteServiceClient,
	staffService staffPb.StaffServiceClient,
	queue *nsq.Producer,
	elasticBulkProcessor *elastic.BulkProcessor,
) *Worker {
	workerLogger := logging.CloneLogrusEntry(logger)
	workerLogger.Data["workerID"] = id

	// Get a new SyncService
	reindexService := class.NewReindexService(workerLogger, queue, elasticBulkProcessor, classService, staffService, siteService)

	return &Worker{
		ID:                   id,
		Config:               &config.Sync,
		Logger:               workerLogger,
		ClassService:         classService,
		SiteService:          siteService,
		StaffService:         staffService,
		Queue:                queue,
		ElasticBulkProcessor: elasticBulkProcessor,
		ReindexService:       reindexService,
	}
}

// HandleMessage handles any queue message
func (w *Worker) HandleMessage(message *nsq.Message) error {
	// Do not auto-finish or requeue messages
	message.DisableAutoResponse()

	// If we cannot unmarshal it into a generic queue message, then drop it
	reindexMessage := &queueHelper.ReindexMessage{}
	err := json.Unmarshal(message.Body, reindexMessage)
	if err != nil {
		w.Logger.Warningf("Unable to unmarshal message: %v", message)
		message.Finish()
		return nil
	}

	if reindexMessage.Status == queueHelper.ReindexStatusStart {
		// Push the message to the according channel
		switch reindexMessage.ObjectType {
		case queueHelper.ObjectTypeSiteClassSchedules:
			w.Buffers[string(reindexMessage.ObjectType)] <- ChanEntry{NSQMessage: message, ReindexMessage: reindexMessage}
		default:
			// If we don't recognize the type, warning and drop the message.
			w.Logger.Warningf("Skipping message with type '%s'.", reindexMessage.ObjectType)
			message.Finish()
		}
	} else {
		// We don't care about the "Done" messages
		message.Finish()
	}

	return nil
}

// Start makes a worker process the channels
func (w *Worker) Start() {
	w.Buffers = map[string]chan ChanEntry{}

	channels := map[string]func([]ChanEntry){
		string(queueHelper.ObjectTypeSiteClassSchedules): w.ProcessSiteClassSchedules,
	}

	for chName, chProcessor := range channels {
		w.startQueueProcessing(chName, chProcessor)
	}
}

func (w *Worker) startQueueProcessing(name string, process func([]ChanEntry)) {
	w.Buffers[name] = make(chan ChanEntry, w.Config.MaxBatchSize*3)

	go func() {
		for {
			select {
			case f := <-w.Buffers[name]:
				process([]ChanEntry{f})
			}
		}
	}()
}

// ProcessSiteClassSchedules processes the sync message for a site's classes
func (w *Worker) ProcessSiteClassSchedules(entries []ChanEntry) {
	entry := entries[0]
	siteUUID := entry.ReindexMessage.Object.ID

	w.Logger.Infof("Processing for site class schedules '%s'", siteUUID)
	entry.NSQMessage.Finish()

	err := w.ReindexService.ReindexSiteClassSchedules(siteUUID, entry.ReindexMessage.IndexName)
	if err != nil {
		w.Logger.Errorf("Failed to sync class schedules for site '%s'. Requeuing...: %v", siteUUID, err)
		entry.NSQMessage.Requeue(30 * time.Second)
		return
	}

	w.Logger.Infof("Finished processing for site class schedules '%s'", siteUUID)
}
