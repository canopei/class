package class

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/canopei/mindbody"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"bitbucket.org/canopei/class/config"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/slices"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// SyncService is the implementation of a sync service
type SyncService struct {
	Logger               *logrus.Entry
	ClassMBClient        *classMb.Class_x0020_ServiceSoap
	SaleMBClient         *saleMb.Sale_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	ClassService         classPb.ClassServiceClient
	SiteService          sitePb.SiteServiceClient
	StaffService         staffPb.StaffServiceClient
	MindbodyConfig       *config.MindbodyConfig
}

// SNGDropInPricingNames are the the names of the pricing option for dropins
var SNGDropInPricingNames = []string{"sng global drop in", "sng global pass"}

const (
	// ClassLevelNameNone represents the name of class level "none"
	ClassLevelNameNone = "None"
)

// NewSyncService creates a new SyncService instance
func NewSyncService(
	logger *logrus.Entry,
	classMBClient *classMb.Class_x0020_ServiceSoap,
	saleMBClient *saleMb.Sale_x0020_ServiceSoap,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
	classService classPb.ClassServiceClient,
	siteService sitePb.SiteServiceClient,
	staffService staffPb.StaffServiceClient,
	mindbodyConfig *config.MindbodyConfig,
) *SyncService {
	return &SyncService{
		Logger:               logger,
		ClassMBClient:        classMBClient,
		SaleMBClient:         saleMBClient,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		ClassService:         classService,
		SiteService:          siteService,
		StaffService:         staffService,
		MindbodyConfig:       mindbodyConfig,
	}
}

// SyncSiteClassSchedules synchronizes the given site class schedules to DB and ES from MB
func (ss *SyncService) SyncSiteClassSchedules(siteUUID string) error {
	ss.Logger.Infof("Will sync site UUID: %s", siteUUID)
	defer timeTrack(ss.Logger, time.Now(), fmt.Sprintf("SyncSiteClassSchedules for '%s' took", siteUUID))

	ctx := context.Background()

	var err error

	// Get the site from DB
	dbSite, err := ss.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		return fmt.Errorf("Cannot fetch the site by UUIDs: %v", err)
	}

	ss.Logger.Debugf("Syncing classes for site MB ID %d.", dbSite.MbId)

	// Load the levels from DB
	levels := map[int32]*classPb.ClassLevel{}
	dbLevels, err := ss.ClassService.GetClassLevelsBySiteID(ctx, &classPb.GetClassLevelsBySiteIDRequest{SiteId: dbSite.Id})
	if err != nil {
		return fmt.Errorf("Cannot fetch the levels from DB for site ID %d: %v", dbSite.Id, err)
	}
	for _, dbLevel := range dbLevels.ClassLevels {
		levels[dbLevel.MbId] = dbLevel
	}

	if len(levels) == 0 {
		// Create the "None" level entry
		ss.Logger.Infof("Creating the None Class Level for site ID %d.", dbSite.Id)

		// Create the level
		dbClassLevel, err := ss.ClassService.CreateClassLevel(ctx, &classPb.ClassLevel{
			MbId:   0,
			SiteId: dbSite.Id,
			Name:   ClassLevelNameNone,
		})
		if err != nil {
			return fmt.Errorf("Failed to create the None Class Level for site ID %d", dbSite.Id)
		}

		levels[0] = dbClassLevel
	}

	// Load the session types from DB
	sessionTypes := map[int32]*classPb.SessionType{}
	dbSessionTypes, err := ss.ClassService.GetSessionTypesBySiteID(ctx, &classPb.GetSessionTypesBySiteIDRequest{SiteId: dbSite.Id})
	if err != nil {
		return fmt.Errorf("Cannot fetch the session types from DB: %v", err)
	}
	for _, dbSessionType := range dbSessionTypes.SessionTypes {
		sessionTypes[dbSessionType.MbId] = dbSessionType
	}

	// Load the programs from DB
	programs := map[int32]*classPb.Program{}
	dbPrograms, err := ss.ClassService.GetProgramsBySiteID(ctx, &classPb.GetProgramsBySiteIDRequest{SiteId: dbSite.Id})
	if err != nil {
		return fmt.Errorf("Cannot fetch the programs from DB for site ID %d: %v", dbSite.Id, err)
	}
	for _, dbProgram := range dbPrograms.Programs {
		programs[dbProgram.MbId] = dbProgram
	}

	// Get the class schedules from MB
	mbRequest := mbHelpers.CreateClassMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	mbRequest.Fields = &classMb.ArrayOfString{String: []string{"ClassSchedules.Classes"}}
	// Fetch the class schedules from Mindbody
	mbClassSchedulesResponse, err := ss.ClassMBClient.GetClassSchedules(&classMb.GetClassSchedules{Request: &classMb.GetClassSchedulesRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		return fmt.Errorf("Cannot fetch the class schedules from Mindbody: %v", err)
	}
	mbClassSchedulesResult := mbClassSchedulesResponse.GetClassSchedulesResult
	if mbClassSchedulesResult.ErrorCode.Int != 200 {
		return fmt.Errorf("Mindbody API response error: %s", mbClassSchedulesResult.Message)
	}

	ss.Logger.Debugf("Found %d class schedules in Mindbody.", len(mbClassSchedulesResult.ClassSchedules.ClassSchedule))
	if len(mbClassSchedulesResult.ClassSchedules.ClassSchedule) == 0 {
		ss.Logger.Info("No class schedules were found.")
	}

	ss.Logger.Debugf("Syncing %d class schedules...", len(mbClassSchedulesResult.ClassSchedules.ClassSchedule))

	// A map for Location ID - ClassDescription MbId
	seenClassDescriptions := map[int32]map[int32]bool{}

	dbStaffCache := map[int64]*staffPb.Staff{}

	for _, xmlClassSchedule := range mbClassSchedulesResult.ClassSchedules.ClassSchedule {
		reindexClassesToES := false

		ss.Logger.Debugf("Syncing Class schedule ID %d", xmlClassSchedule.ID.Int)

		// Fetch the location
		dbLocation, err := ss.SiteService.GetLocationByMBID(ctx, &sitePb.GetLocationRequest{
			Id:       int32(xmlClassSchedule.Location.ID.Int),
			SiteUuid: dbSite.Uuid,
		})
		if err != nil {
			ss.Logger.Warningf("Cannot fetch the location %d from DB", xmlClassSchedule.Location.ID.Int)
			continue
		}

		dbStaff, ok := dbStaffCache[xmlClassSchedule.Staff.ID.Int]
		if !ok {
			// Fetch the staff
			res, err := ss.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{SiteId: dbSite.Id, MbIds: []int64{xmlClassSchedule.Staff.ID.Int}})
			if err != nil {
				return fmt.Errorf("Cannot fetch the staff %d from DB: %v", xmlClassSchedule.Staff.ID.Int, err)
			}
			if len(res.Staff) == 0 {
				ss.Logger.Warningf("Staff MB ID %d not found in DB. Sipping class schedule...", xmlClassSchedule.Staff.ID.Int)
				continue
			}
			dbStaff = res.Staff[0]

			dbStaffCache[xmlClassSchedule.Staff.ID.Int] = dbStaff
		}

		// Get the level
		// 0 should be the MB ID of the "None" class level
		dbClassLevel := &classPb.ClassLevel{}

		*dbClassLevel = *levels[0]
		if xmlClassSchedule.ClassDescription.Level != nil {
			mbClassLevel := mbClassLevelToPbClassLevel(xmlClassSchedule.ClassDescription.Level)
			mbClassLevel.SiteId = dbSite.Id

			tmpClassLevel, ok := levels[mbClassLevel.MbId]
			if !ok {
				ss.Logger.Infof("Creating Class Level %d.", mbClassLevel.MbId)

				// Create the level
				dbClassLevel, err = ss.ClassService.CreateClassLevel(ctx, mbClassLevel)
				if err != nil {
					return fmt.Errorf("Failed to create a new Class Level %d", mbClassLevel.MbId)
				}
				levels[dbClassLevel.MbId] = dbClassLevel
			} else {
				*dbClassLevel = *tmpClassLevel

				// Check if we need updating
				if hasClassLevelChanged(dbClassLevel, mbClassLevel) {
					ss.Logger.Infof("Updating Class Level MB %d (DB %d).", mbClassLevel.MbId, dbClassLevel.Id)

					mbClassLevel.Id = dbClassLevel.Id
					dbClassLevel, err = ss.ClassService.UpdateClassLevel(ctx, mbClassLevel)
					if err != nil {
						return fmt.Errorf("Failed to update the Class Level ID %d", mbClassLevel.Id)
					}
					levels[dbClassLevel.MbId] = dbClassLevel

					reindexClassesToES = true
				}
			}
		}

		// Handle the Program
		mbProgram, err := mbProgramToPbProgram(xmlClassSchedule.ClassDescription.Program)
		mbProgram.SiteId = dbSite.Id
		if err != nil {
			return fmt.Errorf("Failed to convert MB program to PB program (%d): %v", xmlClassSchedule.ClassDescription.Program.ID.Int, err)
		}
		// Get the program from DB
		dbProgram, ok := programs[mbProgram.MbId]
		if !ok {
			ss.Logger.Infof("Creating Class Level %d.", mbProgram.MbId)

			// Create the program
			dbProgram, err = ss.ClassService.CreateProgram(ctx, mbProgram)
			if err != nil {
				return fmt.Errorf("Failed to create a new Program MB %d", mbProgram.MbId)
			}
			programs[dbProgram.MbId] = dbProgram
		} else {
			// Check if we need updating
			if hasProgramChanged(dbProgram, mbProgram) {
				ss.Logger.Infof("Updating Program MB %d (DB %d).", mbProgram.MbId, dbProgram.Id)

				mbProgram.Id = dbProgram.Id
				dbProgram, err := ss.ClassService.UpdateProgram(ctx, mbProgram)
				if err != nil {
					return fmt.Errorf("Failed to update the Program ID %d", mbProgram.Id)
				}
				programs[dbProgram.MbId] = dbProgram

				reindexClassesToES = true
			}
		}

		// Handle the session type
		mbSessionType := mbSessionTypeToPbSessionType(xmlClassSchedule.ClassDescription.SessionType)
		mbSessionType.ProgramId = dbProgram.Id
		mbSessionType.SiteId = dbSite.Id

		// Get the session type from DB
		dbSessionType, ok := sessionTypes[mbSessionType.MbId]
		if !ok {
			ss.Logger.Infof("Creating Session Type MB %d.", mbSessionType.MbId)

			// Create the session type
			dbSessionType, err = ss.ClassService.CreateSessionType(ctx, mbSessionType)
			if err != nil {
				return fmt.Errorf("Failed to create a new Session Type MB %d", mbSessionType.MbId)
			}
			sessionTypes[dbSessionType.MbId] = dbSessionType
		} else {
			// Check if we need updating
			if hasSessionTypeChanged(dbSessionType, mbSessionType) {
				ss.Logger.Infof("Updating Session Type MB %d (DB %d).", mbSessionType.MbId, dbSessionType.Id)

				mbSessionType.Id = dbSessionType.Id
				dbSessionType, err := ss.ClassService.UpdateSessionType(ctx, mbSessionType)
				if err != nil {
					return fmt.Errorf("Failed to update the Session Type ID %d", mbSessionType.Id)
				}
				sessionTypes[dbSessionType.MbId] = dbSessionType

				reindexClassesToES = true
			}
		}
		dbSessionType.Program = dbProgram

		// Handle the class description
		mbClassDescription := mbClassDescriptionToPbClassDescription(xmlClassSchedule.ClassDescription)
		mbClassDescription.SiteId = dbSite.Id
		mbClassDescription.LevelId = dbClassLevel.Id
		mbClassDescription.SessionTypeId = dbSessionType.Id
		// Fetch the class description
		dbClassDescription, err := ss.ClassService.GetClassDescriptionByMBID(ctx, &classPb.GetClassDescriptionByMBIDRequest{
			MbId:   mbClassDescription.MbId,
			SiteId: dbSite.Id,
		})
		if err != nil {
			errorCode := grpc.Code(err)
			if errorCode == codes.NotFound {
				ss.Logger.Infof("Creating Class Description MB %d.", mbClassDescription.MbId)

				dbClassDescription, err = ss.ClassService.CreateClassDescription(ctx, mbClassDescription)
				if err != nil {
					return fmt.Errorf("Failed to create a new Class Description MB %d: %v", mbClassDescription.MbId, err)
				}
			} else {
				return fmt.Errorf("Cannot fetch the class description MB %d from DB", mbClassDescription.MbId)
			}
		} else {
			if hasClassDescriptionChanged(dbClassDescription, mbClassDescription) {
				ss.Logger.Infof("Updating Class description %d (DB %d).", mbClassDescription.MbId, dbClassDescription.Id)

				mbClassDescription.Uuid = dbClassDescription.Uuid
				dbClassDescription, err = ss.ClassService.UpdateClassDescription(ctx, mbClassDescription)
				if err != nil {
					return fmt.Errorf("Failed to update the Class Description DB %d: %v", mbClassDescription.Id, err)
				}

				reindexClassesToES = true
			}
		}
		dbClassDescription.Level = dbClassLevel
		dbClassDescription.SessionType = dbSessionType

		// Add this ClassDescription to the list of seen ones
		if _, ok := seenClassDescriptions[dbLocation.Id]; !ok {
			seenClassDescriptions[dbLocation.Id] = map[int32]bool{}
		}
		seenClassDescriptions[dbLocation.Id][dbClassDescription.MbId] = true

		// Handle the class schedule
		mbClassSchedule := mbClassScheduleToPbClassSchedule(xmlClassSchedule)
		mbClassSchedule.SiteId = dbSite.Id
		mbClassSchedule.LocationId = dbLocation.Id
		mbClassSchedule.StaffId = dbStaff.Id
		mbClassSchedule.ClassDescriptionId = dbClassDescription.Id

		// Fetch the class schedule from DB
		dbClassSchedule, err := ss.ClassService.GetClassScheduleByMBID(ctx, &classPb.GetClassScheduleByMBIDRequest{
			MbId:   mbClassSchedule.MbId,
			SiteId: dbSite.Id,
		})
		if err != nil {
			errorCode := grpc.Code(err)
			if errorCode == codes.NotFound {
				ss.Logger.Infof("Creating Class Schedule MB %d.", mbClassSchedule.MbId)

				dbClassSchedule, err = ss.ClassService.CreateClassSchedule(ctx, mbClassSchedule)
				if err != nil {
					return fmt.Errorf("Failed to create a new Class Schedule MB %d: %v", mbClassSchedule.MbId, err)
				}
			} else {
				return fmt.Errorf("Cannot fetch the class schedule MB %d from DB", mbClassSchedule.MbId)
			}
		} else {
			if hasClassScheduleChanged(dbClassSchedule, mbClassSchedule) {
				ss.Logger.Infof("Updating Class Schedule MB %d (DB %d).", mbClassSchedule.MbId, dbClassSchedule.Id)

				mbClassSchedule.Id = dbClassSchedule.Id
				dbClassSchedule, err = ss.ClassService.UpdateClassSchedule(ctx, mbClassSchedule)
				if err != nil {
					return fmt.Errorf("Failed to update the Class Schedule DB %d: %v", mbClassSchedule.Id, err)
				}

				reindexClassesToES = true
			}
		}
		dbClassSchedule.Location = dbLocation
		dbClassSchedule.Staff = dbStaff
		dbClassSchedule.Description = dbClassDescription

		for _, xmlClass := range xmlClassSchedule.Classes.Class {
			// skip classes more than 3 months in the future
			if xmlClass.StartDateTime.Time.After(time.Now().AddDate(0, 3, 0)) {
				continue
			}

			ss.Logger.Debugf("Syncing Class ID %d", xmlClass.ID.Int)

			indexToES := false

			dbClassStaff, ok := dbStaffCache[xmlClass.Staff.ID.Int]
			if !ok {
				// Fetch the staff
				res, err := ss.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{SiteId: dbSite.Id, MbIds: []int64{xmlClass.Staff.ID.Int}})
				if err != nil {
					return fmt.Errorf("Cannot fetch the staff %d from DB for class: %v", xmlClass.Staff.ID.Int, err)
				}
				if len(res.Staff) == 0 {
					ss.Logger.Warningf("Staff MB ID %d not found in DB for class", xmlClass.Staff.ID.Int)

					dbClassStaff = dbStaff
				} else {
					dbClassStaff = res.Staff[0]

					dbStaffCache[xmlClass.Staff.ID.Int] = dbClassStaff
				}
			}

			mbClass := mbClassToPbClass(xmlClass)
			mbClass.ClassScheduleId = dbClassSchedule.Id
			mbClass.SiteId = dbSite.Id
			mbClass.StaffId = dbClassStaff.Id

			// Fetch the class from DB
			dbClass, err := ss.ClassService.GetClassByMBID(ctx, &classPb.GetClassByMBIDRequest{
				MbId:   mbClass.MbId,
				SiteId: dbSite.Id,
			})
			if err != nil {
				errorCode := grpc.Code(err)
				if errorCode == codes.NotFound {
					ss.Logger.Infof("Creating Class MB %d.", mbClass.MbId)

					dbClass, err = ss.ClassService.CreateClass(ctx, mbClass)
					if err != nil {
						return fmt.Errorf("Failed to create the Class MB %d: %v", mbClass.MbId, err)
					}
					indexToES = true
				} else {
					return fmt.Errorf("Cannot fetch the class %d from DB: %v", mbClass.MbId, err)
				}
			} else {
				if hasClassChanged(dbClass, mbClass) {
					ss.Logger.Infof("Updating Class MB %d (DB %d).", mbClass.MbId, dbClass.Id)

					mbClass.Uuid = dbClass.Uuid
					dbClass, err = ss.ClassService.UpdateClass(ctx, mbClass)
					if err != nil {
						return fmt.Errorf("Failed to update the Class DB %d: %v", mbClass.Id, err)
					}
					indexToES = true
				}
			}
			dbClass.Description = dbClassDescription
			dbClass.Staff = dbClassStaff
			dbClass.ClassSchedule = dbClassSchedule
			dbClass.Location = dbLocation

			if reindexClassesToES || indexToES {
				indexClassToES(ss.ElasticsearchService, "sng", dbLocation.Uuid, dbClass)
			}
		}
	}

	// Cleanup not-seen class descriptions
	for locationID, classDescriptionsMBIDsMap := range seenClassDescriptions {
		classDescriptionMBIDs := []int32{}
		for classDescriptionsMBID := range classDescriptionsMBIDsMap {
			classDescriptionMBIDs = append(classDescriptionMBIDs, classDescriptionsMBID)
		}

		if len(classDescriptionMBIDs) > 0 {
			_, err := ss.ClassService.DeleteClassDescriptionForLocationIDExceptMBIDs(
				ctx,
				&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{
					LocationId: locationID,
					MbIds:      classDescriptionMBIDs,
				},
			)
			if err != nil {
				return fmt.Errorf("Failed to clean the Class descriptions: %v", err)
			}
		}
	}

	ss.Logger.Infof("Updating the programs SNG dropin service MB ID...")
	// Check the DropIn pricing option for programs
	programMBIDs := []*mindbody.CustomInt32{}
	for _, dbProgram := range programs {
		programMBIDs = append(programMBIDs, &mindbody.CustomInt32{Int: dbProgram.MbId})
	}
	ss.Logger.Infof("Got %d programs to check.", len(programMBIDs))

	credsUsername := "_" + ss.MindbodyConfig.SourceName
	credsPassword := ss.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	// Get the available services from MB
	saleMbRequest := mbHelpers.CreateSaleMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	saleMbRequest.UserCredentials = &saleMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: saleMbRequest.SourceCredentials.SiteIDs}
	getServicesRequest := &saleMb.GetServicesRequest{
		MBRequest:  saleMbRequest,
		ProgramIDs: &saleMb.ArrayOfInt{Int: programMBIDs},
	}
	// Fetch the services for class from Mindbody
	mbServicesResponse, err := ss.SaleMBClient.GetServices(&saleMb.GetServices{Request: getServicesRequest})
	if err != nil {
		return fmt.Errorf("Mindbody API minor response error: %#v", err)
	}
	mbServicesResult := mbServicesResponse.GetServicesResult
	if mbServicesResult.ErrorCode.Int != 200 {
		return fmt.Errorf("Mindbody API minor response error: %s", mbServicesResult.Message)
	}
	serviceNames := []string{}
	for _, mbService := range mbServicesResult.Services.Service {
		serviceNames = append(serviceNames, mbService.Name)
	}
	ss.Logger.Infof("Got %d services from Mindbody for site MB ID %d: [%s]", len(mbServicesResult.Services.Service), dbSite.MbId, strings.Join(serviceNames, ","))

	for _, dbProgram := range programs {
		for _, mbService := range mbServicesResult.Services.Service {
			mbID, err := strconv.ParseInt(mbService.ID, 10, 32)
			if err != nil {
				ss.Logger.Errorf("Failed to parse service ID to int32: %#v", err)
				return fmt.Errorf("Failed to parse service ID to int32: %#v", err)
			}

			if mbService.ProgramID.Int == dbProgram.MbId && slices.Scontains(SNGDropInPricingNames, strings.ToLower(strings.TrimSpace(mbService.Name))) {
				if dbProgram.DropinServiceMbId != int32(mbID) {
					dbProgram.DropinServiceMbId = int32(mbID)
					ss.Logger.Infof("Updating program ID %d SNGDropInPricing ID to %d.", dbProgram.Id, dbProgram.DropinServiceMbId)

					dbProgram, err := ss.ClassService.UpdateProgram(ctx, dbProgram)
					if err != nil {
						return fmt.Errorf("Failed to update the Program's SNGDropInPricing (ID %d)", dbProgram.Id)
					}
					programs[dbProgram.MbId] = dbProgram
				}
				break
			}
		}
	}
	ss.Logger.Infof("Finished updating the programs SNG dropin service MB ID...")

	return nil
}

// SyncSiteClasses syncs from MB to DB and ES the given class UUIDs.
// All the classes should belong to the same Site ID.
func (ss *SyncService) SyncSiteClasses(classUUIDs []string) error {
	ss.Logger.Infof("Will sync class UUIDs: %s", strings.Join(classUUIDs, ", "))
	defer timeTrack(ss.Logger, time.Now(), fmt.Sprintf("SyncClasses for %d classes took", len(classUUIDs)))

	ctx := context.Background()

	dbClasses, err := ss.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: strings.Join(classUUIDs, ", ")})
	if err != nil {
		return fmt.Errorf("Failed to fetch the classes from DB: %v", err)
	}
	siteID := dbClasses.Classes[0].SiteId

	// Fetch the site
	dbSite, err := ss.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Id: siteID})
	if err != nil {
		return fmt.Errorf("Failed to fetch the site ID %d from DB: %v", siteID, err)
	}

	// Fetch the site's locations
	dbLocations, err := ss.SiteService.GetSitesLocations(ctx, &sitePb.GetSitesLocationsRequest{Uuids: dbSite.Uuid})
	if err != nil {
		return fmt.Errorf("Cannot fetch the locations for site '%s' from DB.", dbSite.Uuid)
	}
	dbLocationsMap := map[int32]*sitePb.Location{}
	for _, dbLocation := range dbLocations.Locations {
		dbLocationsMap[dbLocation.MbId] = dbLocation
	}

	// Load the levels from DB
	levels := map[int32]*classPb.ClassLevel{}
	dbLevels, err := ss.ClassService.GetClassLevelsBySiteID(ctx, &classPb.GetClassLevelsBySiteIDRequest{SiteId: siteID})
	if err != nil {
		return fmt.Errorf("Cannot fetch the levels from DB for site ID %d: %v", siteID, err)
	}
	for _, dbLevel := range dbLevels.ClassLevels {
		levels[dbLevel.MbId] = dbLevel
	}

	if len(levels) == 0 {
		// Create the "None" level entry
		ss.Logger.Infof("Creating the None Class Level for site ID %d.", siteID)

		// Create the level
		dbClassLevel, err := ss.ClassService.CreateClassLevel(ctx, &classPb.ClassLevel{
			MbId:   0,
			SiteId: siteID,
			Name:   ClassLevelNameNone,
		})
		if err != nil {
			return fmt.Errorf("Failed to create the None Class Level for site ID %d", siteID)
		}

		levels[0] = dbClassLevel
	}

	// Load the session types from DB
	sessionTypes := map[int32]*classPb.SessionType{}
	dbSessionTypes, err := ss.ClassService.GetSessionTypesBySiteID(ctx, &classPb.GetSessionTypesBySiteIDRequest{SiteId: siteID})
	if err != nil {
		return fmt.Errorf("Cannot fetch the session types from DB: %v", err)
	}
	for _, dbSessionType := range dbSessionTypes.SessionTypes {
		sessionTypes[dbSessionType.MbId] = dbSessionType
	}

	// Load the programs from DB
	programs := map[int32]*classPb.Program{}
	dbPrograms, err := ss.ClassService.GetProgramsBySiteID(ctx, &classPb.GetProgramsBySiteIDRequest{SiteId: siteID})
	if err != nil {
		return fmt.Errorf("Cannot fetch the programs from DB for site ID %d: %v", siteID, err)
	}
	for _, dbProgram := range dbPrograms.Programs {
		programs[dbProgram.MbId] = dbProgram
	}

	// Start getting the classes from MB
	classMBIDs := &classMb.ArrayOfInt{Int: []*mindbody.CustomInt32{}}
	for _, dbClass := range dbClasses.Classes {
		classMBIDs.Int = append(classMBIDs.Int, &mindbody.CustomInt32{Int: dbClass.MbId})
	}

	mbRequest := mbHelpers.CreateClassMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)

	credsUsername := "_" + ss.MindbodyConfig.SourceName
	credsPassword := ss.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}
	mbRequest.UserCredentials = &classMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}

	// mbRequest.Fields = &classMb.ArrayOfString{String: []string{"Classes.Resource"}}
	// mbRequest.XMLDetail = &classMb.XMLDetailLevelBasic{}
	mbRequest.PageSize = &mindbody.CustomInt32{Int: 50}
	// Fetch the classes from Mindbody
	mbClassesResponse, err := ss.ClassMBClient.GetClasses(&classMb.GetClasses{Request: &classMb.GetClassesRequest{
		MBRequest:     mbRequest,
		ClassIDs:      classMBIDs,
		StartDateTime: &mindbody.CustomTime{Time: time.Now().AddDate(0, -3, 0)},
		EndDateTime:   &mindbody.CustomTime{Time: time.Now().AddDate(0, 12, 0)},
	}})
	if err != nil {
		return fmt.Errorf("Cannot fetch the classes from Mindbody: %v", err)
	}
	mbClassesResult := mbClassesResponse.GetClassesResult
	if mbClassesResult.ErrorCode.Int != 200 {
		return fmt.Errorf("Mindbody API response error: %s", mbClassesResult.Message)
	}

	ss.Logger.Debugf("Found %d classes in Mindbody.", len(mbClassesResult.Classes.Class))
	if len(mbClassesResult.Classes.Class) == 0 {
		ss.Logger.Info("No classes were found for %v.", classMBIDs.Int)
		// return fmt.Errorf("No classes were found for %v.", classMBIDs.Int)
	} else {
		ss.Logger.Infof("Syncing %d classes found in MB...", len(mbClassesResult.Classes.Class))
	}
	// Done fetching the classes from MB

	// A map for Location ID - ClassDescription MbId
	seenClassMBIDs := []int32{}

	dbStaffCache := map[int64]*staffPb.Staff{}

	for _, xmlClass := range mbClassesResult.Classes.Class {
		reindexClassToES := false

		ss.Logger.Debugf("Syncing Class MB ID %d", xmlClass.ID.Int)

		// Get the location
		dbLocation, ok := dbLocationsMap[xmlClass.Location.ID.Int]
		if !ok {
			return fmt.Errorf("Location MB ID %d not found in DB for class MB ID %d", xmlClass.Location.ID.Int, xmlClass.ID.Int)
		}

		dbStaff, ok := dbStaffCache[xmlClass.Staff.ID.Int]
		if !ok {
			// Fetch the staff
			res, err := ss.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{SiteId: dbSite.Id, MbIds: []int64{xmlClass.Staff.ID.Int}})
			if err != nil {
				return fmt.Errorf("Cannot fetch the staff MB ID %d from DB", xmlClass.Staff.ID.Int)
			}
			if len(res.Staff) == 0 {
				ss.Logger.Warningf("Staff MB ID %d not found in DB. Skipping class...", xmlClass.Staff.ID.Int)
				continue
			}
			dbStaff = res.Staff[0]

			dbStaffCache[xmlClass.Staff.ID.Int] = dbStaff
		}

		// Get the level
		// 0 should be the MB ID of the "None" class level
		dbClassLevel := &classPb.ClassLevel{}

		*dbClassLevel = *levels[0]
		if xmlClass.ClassDescription.Level != nil {
			mbClassLevel := mbClassLevelToPbClassLevel(xmlClass.ClassDescription.Level)
			mbClassLevel.SiteId = dbSite.Id

			tmpClassLevel, ok := levels[mbClassLevel.MbId]
			if !ok {
				ss.Logger.Infof("Creating Class Level %d.", mbClassLevel.MbId)

				// Create the level
				dbClassLevel, err = ss.ClassService.CreateClassLevel(ctx, mbClassLevel)
				if err != nil {
					return fmt.Errorf("Failed to create a new Class Level %d", mbClassLevel.MbId)
				}
				levels[dbClassLevel.MbId] = dbClassLevel
			} else {
				*dbClassLevel = *tmpClassLevel

				// Check if we need updating
				if hasClassLevelChanged(dbClassLevel, mbClassLevel) {
					ss.Logger.Infof("Updating Class Level MB %d (DB %d).", mbClassLevel.MbId, dbClassLevel.Id)

					mbClassLevel.Id = dbClassLevel.Id
					dbClassLevel, err = ss.ClassService.UpdateClassLevel(ctx, mbClassLevel)
					if err != nil {
						return fmt.Errorf("Failed to update the Class Level ID %d", mbClassLevel.Id)
					}
					levels[dbClassLevel.MbId] = dbClassLevel

					reindexClassToES = true
				}
			}
		}

		// Handle the Program
		mbProgram, err := mbProgramToPbProgram(xmlClass.ClassDescription.Program)
		mbProgram.SiteId = dbSite.Id
		if err != nil {
			return fmt.Errorf("Failed to convert MB program to PB program (%d): %v", xmlClass.ClassDescription.Program.ID.Int, err)
		}
		// Get the program from DB
		dbProgram, ok := programs[mbProgram.MbId]
		if !ok {
			ss.Logger.Infof("Creating Class Level %d.", mbProgram.MbId)

			// Create the program
			dbProgram, err = ss.ClassService.CreateProgram(ctx, mbProgram)
			if err != nil {
				return fmt.Errorf("Failed to create a new Program MB %d", mbProgram.MbId)
			}
			programs[dbProgram.MbId] = dbProgram
		} else {
			// Check if we need updating
			if hasProgramChanged(dbProgram, mbProgram) {
				ss.Logger.Infof("Updating Program MB %d (DB %d).", mbProgram.MbId, dbProgram.Id)

				mbProgram.Id = dbProgram.Id
				dbProgram, err := ss.ClassService.UpdateProgram(ctx, mbProgram)
				if err != nil {
					return fmt.Errorf("Failed to update the Program ID %d", mbProgram.Id)
				}
				programs[dbProgram.MbId] = dbProgram

				reindexClassToES = true
			}
		}

		// Handle the session type
		mbSessionType := mbSessionTypeToPbSessionType(xmlClass.ClassDescription.SessionType)
		mbSessionType.ProgramId = dbProgram.Id
		mbSessionType.SiteId = dbSite.Id
		// Get the session type from DB
		dbSessionType, ok := sessionTypes[mbSessionType.MbId]
		if !ok {
			ss.Logger.Infof("Creating Session Type MB %d.", mbSessionType.MbId)

			// Create the session type
			dbSessionType, err = ss.ClassService.CreateSessionType(ctx, mbSessionType)
			if err != nil {
				return fmt.Errorf("Failed to create a new Session Type MB %d", mbSessionType.MbId)
			}
			sessionTypes[dbSessionType.MbId] = dbSessionType
		} else {
			// Check if we need updating
			if hasSessionTypeChanged(dbSessionType, mbSessionType) {
				ss.Logger.Infof("Updating Session Type MB %d (DB %d).", mbSessionType.MbId, dbSessionType.Id)

				mbSessionType.Id = dbSessionType.Id
				dbSessionType, err := ss.ClassService.UpdateSessionType(ctx, mbSessionType)
				if err != nil {
					return fmt.Errorf("Failed to update the Session Type ID %d", mbSessionType.Id)
				}
				sessionTypes[dbSessionType.MbId] = dbSessionType

				reindexClassToES = true
			}
		}
		dbSessionType.Program = dbProgram

		// Handle the class description
		mbClassDescription := mbClassDescriptionToPbClassDescription(xmlClass.ClassDescription)
		mbClassDescription.SiteId = dbSite.Id
		mbClassDescription.LevelId = dbClassLevel.Id
		mbClassDescription.SessionTypeId = dbSessionType.Id
		// Fetch the class description
		dbClassDescription, err := ss.ClassService.GetClassDescriptionByMBID(ctx, &classPb.GetClassDescriptionByMBIDRequest{
			MbId:   mbClassDescription.MbId,
			SiteId: dbSite.Id,
		})

		if err != nil {
			errorCode := grpc.Code(err)
			if errorCode == codes.NotFound {
				ss.Logger.Infof("Creating Class Description MB %d.", mbClassDescription.MbId)

				dbClassDescription, err = ss.ClassService.CreateClassDescription(ctx, mbClassDescription)
				if err != nil {
					return fmt.Errorf("Failed to create a new Class Description MB %d: %v", mbClassDescription.MbId, err)
				}
			} else {
				return fmt.Errorf("Cannot fetch the class description MB %d from DB", mbClassDescription.MbId)
			}
		} else {
			if hasClassDescriptionChanged(dbClassDescription, mbClassDescription) {
				ss.Logger.Infof("Updating Class description %d (DB %d).", mbClassDescription.MbId, dbClassDescription.Id)

				mbClassDescription.Uuid = dbClassDescription.Uuid
				dbClassDescription, err = ss.ClassService.UpdateClassDescription(ctx, mbClassDescription)
				if err != nil {
					return fmt.Errorf("Failed to update the Class Description DB %d: %v", mbClassDescription.Id, err)
				}

				reindexClassToES = true
			}
		}
		dbClassDescription.Level = dbClassLevel
		dbClassDescription.SessionType = dbSessionType

		// Handle the class schedule
		dbClassSchedule, err := ss.ClassService.GetClassScheduleByMBID(ctx, &classPb.GetClassScheduleByMBIDRequest{
			MbId:   xmlClass.ClassScheduleID.Int,
			SiteId: dbSite.Id,
		})
		if err != nil {
			if codes.NotFound == grpc.Code(err) {
				return fmt.Errorf("Cannot find in DB the class schedule MB ID %d", xmlClass.ClassScheduleID.Int)
			}
			return fmt.Errorf("Cannot fetch from DB the class schedule MB ID %d", xmlClass.ClassScheduleID.Int)
		}

		ss.Logger.Debugf("Syncing Class ID %d", xmlClass.ID.Int)

		mbClass := mbClassToPbClass(xmlClass)
		mbClass.ClassScheduleId = dbClassSchedule.Id
		mbClass.SiteId = dbSite.Id
		mbClass.StaffId = dbStaff.Id

		// Fetch the class from DB
		dbClass, err := ss.ClassService.GetClassByMBID(ctx, &classPb.GetClassByMBIDRequest{
			MbId:   mbClass.MbId,
			SiteId: dbSite.Id,
		})
		if err != nil {
			errorCode := grpc.Code(err)
			if errorCode == codes.NotFound {
				ss.Logger.Infof("Creating Class MB %d.", mbClass.MbId)

				dbClass, err = ss.ClassService.CreateClass(ctx, mbClass)
				if err != nil {
					return fmt.Errorf("Failed to create the Class MB %d: %v", mbClass.MbId, err)
				}
				reindexClassToES = true
			} else {
				return fmt.Errorf("Cannot fetch the class %d from DB: %v", mbClass.MbId, err)
			}
		} else {
			if hasClassChanged(dbClass, mbClass) {
				ss.Logger.Infof("Updating Class MB %d (DB %d).", mbClass.MbId, dbClass.Id)

				mbClass.Uuid = dbClass.Uuid
				dbClass, err = ss.ClassService.UpdateClass(ctx, mbClass)
				if err != nil {
					return fmt.Errorf("Failed to update the Class DB %d: %v", mbClass.Id, err)
				}
				reindexClassToES = true
			}
		}

		// Add this ClassDescription to the list of seen ones
		if !slices.Icontains32(seenClassMBIDs, dbClass.MbId) {
			seenClassMBIDs = append(seenClassMBIDs, dbClass.MbId)
		}

		dbClass.Description = dbClassDescription
		dbClass.Staff = dbStaff
		dbClass.ClassSchedule = dbClassSchedule
		dbClass.Location = dbLocation
		if reindexClassToES {
			indexClassToES(ss.ElasticsearchService, "sng", dbLocation.Uuid, dbClass)
		}
	}

	// Delete not seen classes
	for _, dbClass := range dbClasses.Classes {
		if !slices.Icontains32(seenClassMBIDs, dbClass.MbId) {
			ss.ClassService.DeleteClass(ctx, &classPb.DeleteClassRequest{Uuid: dbClass.Uuid})
		}
	}

	return nil
}
