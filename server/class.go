package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// MaxClassesFetched is the limit of number of classes that can be fetched
	// at once.
	MaxClassesFetched int32 = 50

	// DatetimeFormat is the required format for datetimes
	DatetimeFormat = "2006-01-02 15:04:05"
)

// CreateClass creates a Class
func (s *Server) CreateClass(ctx context.Context, req *classPb.Class) (*classPb.Class, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClass: %#v", req)
	defer timeTrack(logger, time.Now(), "CreateClass")

	var err error

	// Validations
	if req.ClassScheduleId < 1 {
		logger.Error("The class schedule ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class schedule ID is required.")
	}
	if req.StartDatetime == "" || req.EndDatetime == "" {
		logger.Error("The start and end date time are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end date time are required.")
	}
	_, err = time.Parse(DatetimeFormat, req.StartDatetime)
	if err != nil {
		logger.Error("Invalid start date time format.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid start date time format.")
	}
	_, err = time.Parse(DatetimeFormat, req.EndDatetime)
	if err != nil {
		logger.Error("Invalid end date time format.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid start end time format.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		if req.MbId < 1 {
			logger.Error("The class MB ID is required.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The class MB ID is required.")
		}
		if req.SiteId < 1 {
			logger.Error("The site ID is required.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
		}

		rows, err := s.DB.Queryx(`SELECT class_id FROM class WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing class.")
		}
		if rows.Next() {
			logger.Errorf("A class already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	// Create the class
	_, err = s.DB.NamedExec(`INSERT INTO class
			(uuid, site_id, mb_id, class_schedule_id, staff_id, max_capacity, web_capacity, total_booked, total_booked_waitlist, web_booked,
			is_canceled, substitute, active, is_available, is_waitlist_available, is_enrolled, hide_cancel, start_datetime,
			end_datetime)
		VALUES (unhex(replace(:uuid,'-','')), :site_id, :mb_id, :class_schedule_id, :staff_id, :max_capacity, :web_capacity, :total_booked,
			:total_booked_waitlist, :web_booked, :is_canceled, :substitute, :active, :is_available, :is_waitlist_available,
			:is_enrolled, :hide_cancel, :start_datetime, :end_datetime)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	var class = &classPb.Class{}
	err = s.DB.Get(class, "SELECT * FROM class WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new class: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new class: %v", err)
		return nil, err
	}
	class.Uuid = req.Uuid

	logger.Debugf("Created class '%s'.", req.Uuid)

	return class, nil
}

// GetClassByMBID retrieves a Class by MB ID
func (s *Server) GetClassByMBID(ctx context.Context, req *classPb.GetClassByMBIDRequest) (*classPb.Class, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassByMBID")

	// Validations
	if req.MbId < 1 {
		logger.Error("The class MBID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class MBID is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	var class classPb.Class
	err := s.DB.Get(&class, `SELECT *
		FROM class
		WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class")
	}

	unpackedUUID, err := crypto.Parse([]byte(class.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	class.Uuid = unpackedUUID.String()

	return &class, nil
}

// GetClasses fetches a list of classes
func (s *Server) GetClasses(ctx context.Context, req *classPb.GetClassesRequest) (*classPb.ClassesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClasses: %v", req)
	defer timeTrack(logger, time.Now(), "GetClasses")

	if req.Limit == 0 || req.Limit > MaxClassesFetched {
		req.Limit = MaxClassesFetched
	}

	req.Uuids = strings.TrimSpace(req.Uuids)
	reqUUIDs := map[string]bool{}
	if req.Uuids != "" {
		uuids := strings.Split(req.Uuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" {
				reqUUIDs[uuid] = true
			}
		}
	}

	logger.Debugf("Fetching %d classes, offset %d...", req.Limit, req.Offset)

	// Fetch the classes
	queryParams := map[string]interface{}{
		"limit":   req.Limit,
		"offset":  req.Offset,
		"site_id": req.SiteId,
	}

	condition := ""
	if len(reqUUIDs) > 0 {
		condition = " AND c.uuid IN ("
		idx := 0
		for v := range reqUUIDs {
			paramKey := "id" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
			idx++
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	if len(req.MbIds) > 0 && req.SiteId > 0 {
		condition = "c.mb_id IN ("
		for idx, v := range req.MbIds {
			paramKey := "mbid" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
		condition = " AND (" + condition + " AND c.site_id = :site_id) "
	}

	if len(req.Ids) > 0 {
		condition = " AND c.class_id IN ("
		for idx, v := range req.Ids {
			paramKey := "classid" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	rows, err := s.DB.NamedQuery(`SELECT
			c.*,
			cs.location_id,
			st.session_type_id "class_description.session_type.session_type_id",
			st.mb_id "class_description.session_type.mb_id",
			st.name "class_description.session_type.name",
			p.program_id "class_description.session_type.program.program_id",
			p.dropin_service_mb_id "class_description.session_type.program.dropin_service_mb_id",
			p.name "class_description.session_type.program.name",
			p.schedule_type "class_description.session_type.program.schedule_type",
			cd.uuid "class_description.uuid",
			cd.name "class_description.name",
			cd.description "class_description.description",
			cd.prereq "class_description.prereq",
			cd.notes "class_description.notes",
			cd.class_level_id "class_description.class_level_id",
			cl.class_level_id "class_level.class_level_id",
			cl.name "class_level.name",
			cl.description "class_level.description"
		FROM class c
		INNER JOIN class_schedule cs ON cs.class_schedule_id = c.class_schedule_id
		INNER JOIN class_description cd ON cs.class_description_id = cd.class_description_id
		INNER JOIN class_level cl ON cl.class_level_id = cd.class_level_id
		INNER JOIN session_type st ON st.session_type_id = cd.session_type_id
		INNER JOIN program p ON p.program_id = st.program_id
		WHERE c.deleted_at = '0'
			AND cd.deleted_at = '0'
			`+condition+`
		ORDER BY c.class_id DESC
		LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the classes")
	}

	classesList := &classPb.ClassesList{}
	for rows.Next() {
		class := classPb.Class{}
		err := rows.StructScan(&class)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the classes")
		}

		// Unpack class UUID
		unpackedUUID, err := crypto.Parse([]byte(class.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for class ID %d", class.Id)
		}
		class.Uuid = unpackedUUID.String()

		// Unpack class description UUID
		unpackedUUID, err = crypto.Parse([]byte(class.Description.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		class.Description.Uuid = unpackedUUID.String()

		classesList.Classes = append(classesList.Classes, &class)
	}

	logger.Debugf("Found %d classes out of %d.", len(classesList.Classes), len(reqUUIDs)+len(req.MbIds))

	return classesList, nil
}

// GetClassesByLocationID fetches a list of classes for a given location
func (s *Server) GetClassesByLocationID(ctx context.Context, req *classPb.GetClassesByLocationIDRequest) (*classPb.ClassesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassesByLocationID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassesByLocationID")

	// Validations
	if req.LocationId < 1 {
		logger.Error("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}
	// validate datetime formats
	if req.From != "" {
		_, err := time.Parse(DatetimeFormat, req.From)
		if err != nil {
			logger.Error("Invalid datetime format for 'from'.")
			return nil, grpc.Errorf(codes.InvalidArgument, "Invalid datetime format for 'from'.")
		}
	}
	if req.To != "" {
		_, err := time.Parse(DatetimeFormat, req.To)
		if err != nil {
			logger.Error("Invalid datetime format for 'to'.")
			return nil, grpc.Errorf(codes.InvalidArgument, "Invalid datetime format for 'to'.")
		}
	}

	// Fetch the classes
	queryParams := map[string]interface{}{
		"limit":       MaxClassesFetched,
		"location_id": req.LocationId,
	}

	condition := ""
	if req.From != "" && req.To != "" {
		condition += " AND c.start_datetime BETWEEN :from AND :to"
		queryParams["from"] = req.From
		queryParams["to"] = req.To
	} else {
		if req.From != "" {
			condition += " AND c.start_datetime >= :from"
			queryParams["from"] = req.From
		}
		if req.To != "" {
			condition += " AND c.start_datetime <= :to"
			queryParams["to"] = req.To
		}
	}

	logger.Debugf("Fetching classes for location ID %d: %v", req.LocationId, queryParams)

	rows, err := s.DB.NamedQuery(`SELECT
			c.*,
			cs.location_id,
			st.session_type_id "class_description.session_type.session_type_id",
			st.name "class_description.session_type.name",
			p.program_id "class_description.session_type.program.program_id",
			p.dropin_service_mb_id "class_description.session_type.program.dropin_service_mb_id",
			p.name "class_description.session_type.program.name",
			p.schedule_type "class_description.session_type.program.schedule_type",
			cd.uuid "class_description.uuid",
			cd.name "class_description.name",
			cd.description "class_description.description",
			cd.prereq "class_description.prereq",
			cd.notes "class_description.notes",
			cd.class_level_id "class_description.class_level_id",
			cl.class_level_id "class_level.class_level_id",
			cl.name "class_level.name",
			cl.description "class_level.description"
		FROM class c
		INNER JOIN class_schedule cs ON cs.class_schedule_id = c.class_schedule_id
		INNER JOIN class_description cd ON cs.class_description_id = cd.class_description_id
		INNER JOIN class_level cl ON cl.class_level_id = cd.class_level_id
		INNER JOIN session_type st ON st.session_type_id = cd.session_type_id
		INNER JOIN program p ON p.program_id = st.program_id
		WHERE c.deleted_at = '0'
			AND cd.deleted_at = '0'
			AND cs.location_id = :location_id `+condition+`
		ORDER BY c.start_datetime DESC
		LIMIT :limit`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the classes")
	}

	classesList := &classPb.ClassesList{}
	for rows.Next() {
		class := classPb.Class{}
		err := rows.StructScan(&class)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the classes")
		}

		// Unpack the class UUID
		unpackedUUID, err := crypto.Parse([]byte(class.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for class ID %d", class.Id)
		}
		class.Uuid = unpackedUUID.String()

		// Unpack the class description UUID
		unpackedUUID, err = crypto.Parse([]byte(class.Description.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for class description.")
		}
		class.Description.Uuid = unpackedUUID.String()

		classesList.Classes = append(classesList.Classes, &class)
	}

	return classesList, nil
}

// UpdateClass updates a Class
func (s *Server) UpdateClass(ctx context.Context, req *classPb.Class) (*classPb.Class, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClass: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClass")

	var err error

	// Validations
	if req.Uuid == "" {
		logger.Error("The class UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class UUID is required.")
	}
	if req.StartDatetime == "" || req.EndDatetime == "" {
		logger.Error("The start and end date time are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end date time are required.")
	}

	_, err = time.Parse(DatetimeFormat, req.StartDatetime)
	if err != nil {
		logger.Error("Invalid start date time format.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid start date time format.")
	}
	_, err = time.Parse(DatetimeFormat, req.EndDatetime)
	if err != nil {
		logger.Error("Invalid end date time format.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid start end time format.")
	}

	_, err = s.DB.NamedExec(`UPDATE class SET
			staff_id = :staff_id, class_schedule_id = :class_schedule_id,
			max_capacity = :max_capacity, web_capacity = :web_capacity, total_booked = :total_booked,
			total_booked_waitlist = :total_booked_waitlist, web_booked = :web_booked, is_canceled = :is_canceled,
			substitute = :substitute, active = :active, is_available = :is_available, is_waitlist_available = :is_waitlist_available,
			is_enrolled = :is_enrolled, hide_cancel = :hide_cancel, start_datetime = :start_datetime, end_datetime = :end_datetime
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var class classPb.Class
	err = s.DB.Get(&class, "SELECT * FROM class WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClass - class not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	class.Uuid = req.Uuid

	return &class, nil
}

// DeleteClass removes a Class (sets deleted_at)
func (s *Server) DeleteClass(ctx context.Context, req *classPb.DeleteClassRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClass: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClass")

	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The class UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class UUID is required.")
	}

	result, err := s.DB.Exec(`UPDATE class SET deleted_at = CURRENT_TIMESTAMP
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))`, req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClass - class not found for '%s'", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
