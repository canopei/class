package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	classLevelColumns = []string{"class_level_id", "site_id", "mb_id", "name", "description", "created_at", "modified_at", "deleted_at"}
)

func TestCreateClassLevelValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassLevel
		ExpectedCode codes.Code
	}{
		{&classPb.ClassLevel{}, codes.InvalidArgument},
		{&classPb.ClassLevel{Name: " "}, codes.InvalidArgument},
		{&classPb.ClassLevel{Name: "foo", MbId: 123}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassLevel{Name: "foo", MbId: 123, SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		level, err := server.CreateClassLevel(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(level, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClassLevelSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(sqlmock.NewRows([]string{"class_level_id"}))
	mock.ExpectExec("^INSERT INTO class_level").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(classLevelColumns).
		AddRow(1, 1, 123, "name", "description", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(rows)

	level, err := server.CreateClassLevel(context.Background(), &classPb.ClassLevel{
		Name:        "name",
		Description: "description",
		MbId:        123,
		SiteId:      1,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(level)
	if level != nil {
		assert.Equal("name", level.Name)
	}
}

func TestCreateClassLevelExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(sqlmock.NewRows([]string{"class_level_id"}).AddRow(1))

	level, err := server.CreateClassLevel(context.Background(), &classPb.ClassLevel{
		Name:        "name",
		Description: "description",
		MbId:        123,
		SiteId:      1,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(level)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetClassLevelValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassLevelRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ClassLevelRequest{}, codes.InvalidArgument},
		{&classPb.ClassLevelRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.ClassLevelRequest{Id: -2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassLevelRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		level, err := server.GetClassLevel(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(level, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassLevelSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classLevelColumns).AddRow(1, 1, 123, "name", "description", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_level WHERE").WithArgs(1).WillReturnRows(rows)

	level, err := server.GetClassLevel(context.Background(), &classPb.ClassLevelRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(level)
	if level != nil {
		assert.Equal(level.Name, "name")
	}
}

func TestGetClassLevelMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_level WHERE").WithArgs(1).WillReturnRows(sqlmock.NewRows(classLevelColumns))

	level, err := server.GetClassLevel(context.Background(), &classPb.ClassLevelRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(level)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassLevelsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	levels := []*classPb.ClassLevel{
		&classPb.ClassLevel{Id: 1, MbId: 123, Name: "foo", SiteId: 1},
		&classPb.ClassLevel{Id: 2, MbId: 124, Name: "bar", SiteId: 1},
		&classPb.ClassLevel{Id: 3, MbId: 125, Name: "foobar", SiteId: 1},
	}
	levelsRows := sqlmock.NewRows(classLevelColumns)
	for _, lev := range levels {
		levelsRows.AddRow(lev.Id, lev.SiteId, lev.MbId, lev.Name, "description", "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(levelsRows)

	response, err := server.GetClassLevels(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassLevels, len(levels))
	}
}

func TestGetClassLevelsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(sqlmock.NewRows(classLevelColumns))

	response, err := server.GetClassLevels(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassLevels, 0)
	}
}

func TestGetClassLevelsBySiteIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassLevelsBySiteIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassLevelsBySiteIDRequest{}, codes.InvalidArgument},
		{&classPb.GetClassLevelsBySiteIDRequest{SiteId: 0}, codes.InvalidArgument},
		{&classPb.GetClassLevelsBySiteIDRequest{SiteId: -2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassLevelsBySiteIDRequest{SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetClassLevelsBySiteID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassLevelsBySiteIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	levels := []*classPb.ClassLevel{
		&classPb.ClassLevel{Id: 1, MbId: 123, SiteId: 1, Name: "foo"},
		&classPb.ClassLevel{Id: 2, MbId: 124, SiteId: 1, Name: "bar"},
		&classPb.ClassLevel{Id: 3, MbId: 125, SiteId: 1, Name: "foobar"},
	}
	levelsRows := sqlmock.NewRows(classLevelColumns)
	for _, lev := range levels {
		levelsRows.AddRow(lev.Id, lev.SiteId, lev.MbId, lev.Name, "description", "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(levelsRows)

	response, err := server.GetClassLevelsBySiteID(context.Background(), &classPb.GetClassLevelsBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassLevels, len(levels))
	}
}

func TestGetClassLevelsBySiteIDEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_level").WillReturnRows(sqlmock.NewRows(classLevelColumns))

	response, err := server.GetClassLevelsBySiteID(context.Background(), &classPb.GetClassLevelsBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassLevels, 0)
	}
}

func TestUpdateClassLevelValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassLevel
		ExpectedCode codes.Code
	}{
		{&classPb.ClassLevel{}, codes.InvalidArgument},
		{&classPb.ClassLevel{Name: "foo"}, codes.InvalidArgument},
		{&classPb.ClassLevel{Id: 1}, codes.InvalidArgument},
		{&classPb.ClassLevel{Id: 1, Name: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassLevel{Id: 1, Name: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		level, err := server.UpdateClassLevel(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(level, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClassLevelMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE class_level SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM class_level WHERE").WithArgs(1).WillReturnRows(sqlmock.NewRows(classLevelColumns))

	level, err := server.UpdateClassLevel(context.Background(), &classPb.ClassLevel{Id: 1, Name: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(level)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClassLevelSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClassLevel := &classPb.ClassLevel{
		Id:          1,
		MbId:        123,
		Name:        "name",
		Description: "description",
	}

	mock.ExpectExec("^UPDATE class_level SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(classLevelColumns).
		AddRow(1, 1, 123, "name", "description", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_level WHERE").WithArgs(testClassLevel.Id).WillReturnRows(rows)

	level, err := server.UpdateClassLevel(context.Background(), testClassLevel)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(level)
	// sanity check
	if level != nil {
		assert.Equal("name", level.Name)
	}
}

func TestDeleteClassLevelValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassLevelRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ClassLevelRequest{}, codes.InvalidArgument},
		{&classPb.ClassLevelRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.ClassLevelRequest{Id: -2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassLevelRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		level, err := server.DeleteClassLevel(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(level, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClassLevel(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE class_level SET(.+)deleted_at").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClassLevel(context.Background(), &classPb.ClassLevelRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE class_level SET(.+)deleted_at").WithArgs(1).WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteClassLevel(context.Background(), &classPb.ClassLevelRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
