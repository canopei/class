package main

import (
	"database/sql"
	"time"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateBooking creates a new Booking
func (s *Server) CreateBooking(ctx context.Context, req *classPb.Booking) (*classPb.Booking, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateBooking: %v", req)
	defer timeTrack(logger, time.Now(), "CreateBooking")

	// Validations
	if req.ClassId < 1 {
		logger.Errorf("The class ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class ID is required.")
	}
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.SiteId < 1 {
		logger.Errorf("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.LocationId < 1 {
		logger.Errorf("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}
	if req.AccountMembershipId < 1 && req.ServiceId < 1 {
		logger.Errorf("One of the account membership ID or the service ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "One of the account membership ID or the service ID is required.")
	}

	req.Status = "active"

	result, err := s.DB.NamedExec(`INSERT INTO booking
			(class_id, account_id, site_id, location_id, account_membership_id, membership_service_id, service_id, status)
		VALUES (:class_id, :account_id, :site_id, :location_id, :account_membership_id, :membership_service_id, :service_id, :status)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	logger.Debugf("Created new booking ID %d.", lastInsertID)

	var booking = &classPb.Booking{}
	err = s.DB.Get(booking, "SELECT * FROM booking WHERE booking_id = ?", lastInsertID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new booking: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new booking: %v", err)
		return nil, err
	}

	return booking, nil
}

// GetBookingForAccountAndClass fetches a booking for the given class ID and account ID
func (s *Server) GetBookingForAccountAndClass(ctx context.Context, req *classPb.Booking) (*classPb.Booking, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetBookingForAccountAndClass: %v", req)
	defer timeTrack(logger, time.Now(), "GetBookingForAccountAndClass")

	// Validations
	if req.AccountId < 1 {
		logger.Error("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.ClassId < 1 {
		logger.Error("The class ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class ID is required.")
	}

	var booking classPb.Booking
	err := s.DB.Get(&booking, "SELECT * FROM booking WHERE account_id = ? AND class_id = ? ORDER BY booking_id DESC LIMIT 1", req.AccountId, req.ClassId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("GetBookingForAccountAndClass - booking not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &booking, nil
}

// UpdateBooking updates a Booking
func (s *Server) UpdateBooking(ctx context.Context, req *classPb.Booking) (*classPb.Booking, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateBooking: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateBooking")

	// Validations
	if req.Id < 1 {
		logger.Error("The booking ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The booking ID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE booking SET
			status = :status
		WHERE booking_id = :booking_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var booking classPb.Booking
	err = s.DB.Get(&booking, "SELECT * FROM booking WHERE booking_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateBooking - booking not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &booking, nil
}
