package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	programColumns = []string{"program_id", "site_id", "mb_id", "name", "dropin_service_mb_id", "schedule_type", "cancel_offset", "created_at", "modified_at", "deleted_at"}
)

func TestCreateProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Program
		ExpectedCode codes.Code
	}{
		{&classPb.Program{}, codes.InvalidArgument},
		{&classPb.Program{Name: " "}, codes.InvalidArgument},
		{&classPb.Program{Name: "bar"}, codes.InvalidArgument},
		{&classPb.Program{Name: "foo", MbId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.Program{Name: "bar", SiteId: 1}, codes.Unknown},
		{&classPb.Program{Name: "foo", MbId: 1, SiteId: 1}, codes.Unknown},
		{&classPb.Program{Name: "foo", MbId: 0, SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		program, err := server.CreateProgram(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(program, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateProgramSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(sqlmock.NewRows([]string{"program_id"}))
	mock.ExpectExec("^INSERT INTO program").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(programColumns).AddRow(1, 1, 123, "name", 12, 1, 5, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(rows)

	program, err := server.CreateProgram(context.Background(), &classPb.Program{
		Name:         "name",
		MbId:         123,
		SiteId:       1,
		CancelOffset: 5,
		ScheduleType: 2,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(program)
	if program != nil {
		assert.Equal("name", program.Name)
	}
}

func TestCreateProgramExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(sqlmock.NewRows([]string{"program_id"}).AddRow(1))

	program, err := server.CreateProgram(context.Background(), &classPb.Program{
		Name:         "name",
		MbId:         123,
		SiteId:       1,
		CancelOffset: 5,
		ScheduleType: 2,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(program)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ProgramRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ProgramRequest{}, codes.InvalidArgument},
		{&classPb.ProgramRequest{Id: -2}, codes.InvalidArgument},
		{&classPb.ProgramRequest{Id: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ProgramRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		program, err := server.GetProgram(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(program, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetProgramSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(programColumns).AddRow(1, 1, 123, "name", 12, 1, 5, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM program WHERE").WillReturnRows(rows)

	program, err := server.GetProgram(context.Background(), &classPb.ProgramRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(program)
	if program != nil {
		assert.Equal("name", program.Name)
	}
}

func TestGetProgramMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(sqlmock.NewRows(programColumns))

	program, err := server.GetProgram(context.Background(), &classPb.ProgramRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(program)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetProgramsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	programs := []*classPb.Program{
		&classPb.Program{Id: 1, MbId: 123, Name: "foo", CancelOffset: 10, ScheduleType: 2},
		&classPb.Program{Id: 2, MbId: 124, Name: "bar", CancelOffset: 15, ScheduleType: 2},
		&classPb.Program{Id: 3, MbId: 125, Name: "foobar", CancelOffset: 10, ScheduleType: 3},
	}
	programsRows := sqlmock.NewRows(programColumns)
	for _, pr := range programs {
		programsRows.AddRow(pr.Id, 1, pr.MbId, pr.Name, 12, pr.ScheduleType, pr.CancelOffset, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(programsRows)

	response, err := server.GetPrograms(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Programs, len(programs))
		// Sanity check
		assert.Equal(int32(15), response.Programs[1].CancelOffset)
	}
}

func TestGetProgramsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(sqlmock.NewRows(programColumns))

	response, err := server.GetPrograms(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Programs, 0)
	}
}

func TestGetProgramsBySiteIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetProgramsBySiteIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetProgramsBySiteIDRequest{}, codes.InvalidArgument},
		{&classPb.GetProgramsBySiteIDRequest{SiteId: 0}, codes.InvalidArgument},
		{&classPb.GetProgramsBySiteIDRequest{SiteId: -2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetProgramsBySiteIDRequest{SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetProgramsBySiteID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetProgramsBySiteIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	programs := []*classPb.Program{
		&classPb.Program{Id: 1, MbId: 123, Name: "foo", CancelOffset: 10, ScheduleType: 2},
		&classPb.Program{Id: 2, MbId: 124, Name: "bar", CancelOffset: 15, ScheduleType: 2},
		&classPb.Program{Id: 3, MbId: 125, Name: "foobar", CancelOffset: 10, ScheduleType: 3},
	}
	programsRows := sqlmock.NewRows(programColumns)
	for _, pr := range programs {
		programsRows.AddRow(pr.Id, 1, pr.MbId, pr.Name, 12, pr.ScheduleType, pr.CancelOffset, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(programsRows)

	response, err := server.GetProgramsBySiteID(context.Background(), &classPb.GetProgramsBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Programs, len(programs))
	}
}

func TestGetProgramsBySiteIDEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM program").WillReturnRows(sqlmock.NewRows(programColumns))

	response, err := server.GetProgramsBySiteID(context.Background(), &classPb.GetProgramsBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Programs, 0)
	}
}

func TestUpdateProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Program
		ExpectedCode codes.Code
	}{
		{&classPb.Program{}, codes.InvalidArgument},
		{&classPb.Program{Id: 1}, codes.InvalidArgument},
		{&classPb.Program{Name: "foo"}, codes.InvalidArgument},
		{&classPb.Program{Id: 1, Name: " "}, codes.InvalidArgument},
		{&classPb.Program{Id: 0, Name: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.Program{Id: 1, Name: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		program, err := server.UpdateProgram(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(program, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateProgramMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE program SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM program WHERE").WillReturnRows(sqlmock.NewRows(programColumns))

	program, err := server.UpdateProgram(context.Background(), &classPb.Program{Id: 1, Name: "foo", CancelOffset: 10, ScheduleType: 2})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(program)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateProgramSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testProgram := &classPb.Program{
		Id:           1,
		Name:         "name",
		CancelOffset: 10,
		ScheduleType: 2,
	}

	mock.ExpectExec("^UPDATE program SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(programColumns).AddRow(1, 1, 123, "name", 12, 2, 10, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM program WHERE").WithArgs(testProgram.Id).WillReturnRows(rows)

	program, err := server.UpdateProgram(context.Background(), testProgram)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(program)
	// sanity check
	if program != nil {
		assert.Equal("name", program.Name)
	}
}

func TestDeleteProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ProgramRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ProgramRequest{}, codes.InvalidArgument},
		{&classPb.ProgramRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.ProgramRequest{Id: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ProgramRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		program, err := server.DeleteProgram(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(program, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteProgram(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE program SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteProgram(context.Background(), &classPb.ProgramRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE program SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteProgram(context.Background(), &classPb.ProgramRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
