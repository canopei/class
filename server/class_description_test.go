package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	classDescriptionColumns = []string{"class_description_id", "site_id", "mb_id", "uuid", "name", "description", "class_level_id",
		"session_type_id", "prereq", "notes", "image_url", "active", "created_at", "modified_at", "deleted_at"}
)

func TestCreateClassDescriptionValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassDescription
		ExpectedCode codes.Code
	}{
		{&classPb.ClassDescription{}, codes.InvalidArgument},
		{&classPb.ClassDescription{Description: "bar"}, codes.InvalidArgument},
		{&classPb.ClassDescription{Name: " "}, codes.InvalidArgument},
		{&classPb.ClassDescription{SiteId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassDescription{SiteId: 1, Name: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.CreateClassDescription(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClassDescriptionSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(sqlmock.NewRows([]string{"class_description_id"}))
	mock.ExpectExec("^INSERT INTO class_description").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(classDescriptionColumns).
		AddRow(1, 1, 123, "1234567890123456", "name", "description", 1, 1, "prereq", "notes", "image_url",
			true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(rows)

	classDesc, err := server.CreateClassDescription(context.Background(), &classPb.ClassDescription{
		SiteId:      1,
		Name:        "name",
		MbId:        123,
		Description: "description",
		LevelId:     1,
		Active:      true,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classDesc)
	if classDesc != nil {
		assert.NotEmpty(classDesc.Uuid)
		assert.Equal("name", classDesc.Name)
	}
}

func TestCreateClassDescriptionExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(sqlmock.NewRows([]string{"class_description_id"}).AddRow(1))

	classDesc, err := server.CreateClassDescription(context.Background(), &classPb.ClassDescription{
		SiteId:      1,
		Name:        "name",
		MbId:        123,
		Description: "description",
		LevelId:     1,
		Active:      true,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classDesc)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetClassDescriptionValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassDescriptionRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassDescriptionRequest{}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionRequest{Id: -2}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionRequest{Uuid: " "}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionRequest{Uuid: "uuid", Id: 2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassDescriptionRequest{Id: 1}, codes.Unknown},
		{&classPb.GetClassDescriptionRequest{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.GetClassDescription(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassDescriptionSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classDescriptionColumns).AddRow(1, 1, 123, "1234567890123456", "name", "description", 1, 1, "prereq", "notes", "image_url",
		true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_description WHERE").WillReturnRows(rows)

	classDesc, err := server.GetClassDescription(context.Background(), &classPb.GetClassDescriptionRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classDesc)
	if classDesc != nil {
		assert.Equal(classDesc.Name, "name")
	}
}

func TestGetClassDescriptionMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(sqlmock.NewRows(classDescriptionColumns))

	classDesc, err := server.GetClassDescription(context.Background(), &classPb.GetClassDescriptionRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classDesc)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassDescriptionByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassDescriptionByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassDescriptionByMBIDRequest{}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionByMBIDRequest{MbId: 0}, codes.InvalidArgument},
		{&classPb.GetClassDescriptionByMBIDRequest{MbId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassDescriptionByMBIDRequest{MbId: 1, SiteId: 1}, codes.Unknown},
		{&classPb.GetClassDescriptionByMBIDRequest{MbId: -1, SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.GetClassDescriptionByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassDescriptionByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classDescriptionColumns).AddRow(1, 1, 123, "1234567890123456", "name", "description", 1, 1, "prereq", "notes", "image_url",
		true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(rows)

	classDesc, err := server.GetClassDescriptionByMBID(context.Background(), &classPb.GetClassDescriptionByMBIDRequest{MbId: 1, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classDesc)
	if classDesc != nil {
		assert.Equal(classDesc.Name, "name")
	}
}

func TestGetClassDescriptionByMBIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(sqlmock.NewRows(classDescriptionColumns))

	classDesc, err := server.GetClassDescriptionByMBID(context.Background(), &classPb.GetClassDescriptionByMBIDRequest{MbId: 1, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classDesc)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassDescriptionsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassDescriptionsRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassDescriptionsRequest{Ids: []string{" "}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassDescriptionsRequest{}, codes.Unknown},
		{&classPb.GetClassDescriptionsRequest{Limit: 10, Offset: 15}, codes.Unknown},
		{&classPb.GetClassDescriptionsRequest{Ids: []string{}}, codes.Unknown},
		{&classPb.GetClassDescriptionsRequest{Ids: []string{"uuid1", "uuid2"}}, codes.Unknown},
		{&classPb.GetClassDescriptionsRequest{Ids: []string{"uuid1", "uuid2", " "}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetClassDescriptions(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassDescriptionsSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	descriptions := []*classPb.ClassDescription{
		&classPb.ClassDescription{Id: 1, MbId: 123, Name: "foo", Prereq: "prereq1", Active: false},
		&classPb.ClassDescription{Id: 2, MbId: 124, Name: "bar", Prereq: "prereq2", Active: true},
		&classPb.ClassDescription{Id: 3, MbId: 125, Name: "foobar", Prereq: "prereq3", Active: true},
	}
	descriptionsRows := sqlmock.NewRows(classDescriptionColumns)
	for _, desc := range descriptions {
		descriptionsRows.AddRow(desc.Id, 1, desc.MbId, "1234567890123456", desc.Name, desc.Description, 1, 1, desc.Prereq, "notes", "image_url", desc.Active, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(descriptionsRows)

	response, err := server.GetClassDescriptions(context.Background(), &classPb.GetClassDescriptionsRequest{Offset: 10, Limit: 15, Ids: []string{"uuid1", "uuid2", " "}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassDescriptions, len(descriptions))
		// Sanity check
		assert.Equal("prereq2", response.ClassDescriptions[1].Prereq)
	}
}

func TestGetClassDescriptionsEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_description").WillReturnRows(sqlmock.NewRows(classDescriptionColumns))

	response, err := server.GetClassDescriptions(context.Background(), &classPb.GetClassDescriptionsRequest{Offset: 10, Limit: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.ClassDescriptions, 0)
	}
}

func TestUpdateClassDescriptionValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassDescription
		ExpectedCode codes.Code
	}{
		{&classPb.ClassDescription{}, codes.InvalidArgument},
		{&classPb.ClassDescription{Uuid: "uuid"}, codes.InvalidArgument},
		{&classPb.ClassDescription{Name: "foo"}, codes.InvalidArgument},
		{&classPb.ClassDescription{Name: "foo", Uuid: " "}, codes.InvalidArgument},
		{&classPb.ClassDescription{Name: " ", Uuid: "uuid"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassDescription{Uuid: "uuid", Name: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classDesc, err := server.UpdateClassDescription(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classDesc, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClassDescriptionMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE class_description SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM class_description WHERE").WithArgs("uuid").WillReturnRows(sqlmock.NewRows(classDescriptionColumns))

	classDesc, err := server.UpdateClassDescription(context.Background(), &classPb.ClassDescription{Uuid: "uuid", Name: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classDesc)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClassDescriptionSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClassDescription := &classPb.ClassDescription{
		Uuid:        "1234567890123456",
		MbId:        123,
		Name:        "name",
		Description: "description",
	}

	mock.ExpectExec("^UPDATE class_description SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(classDescriptionColumns).AddRow(1, 1, 123, "1234567890123456", "name", "description", 1, 1, "prereq", "notes", "image_url",
		true, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM class_description WHERE").WithArgs(testClassDescription.Uuid).WillReturnRows(rows)

	classDesc, err := server.UpdateClassDescription(context.Background(), testClassDescription)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classDesc)
	// sanity check
	if classDesc != nil {
		assert.Equal("name", classDesc.Name)
	}
}

func TestDeleteClassDescriptionValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.DeleteClassDescriptionRequest
		ExpectedCode codes.Code
	}{
		{&classPb.DeleteClassDescriptionRequest{}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionRequest{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.DeleteClassDescriptionRequest{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		level, err := server.DeleteClassDescription(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(level, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClassDescription(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE class_description SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClassDescription(context.Background(), &classPb.DeleteClassDescriptionRequest{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE class_description SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteClassDescription(context.Background(), &classPb.DeleteClassDescriptionRequest{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestDeleteClassDescriptionForLocationIDExceptMBIDsValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest
		ExpectedCode codes.Code
	}{
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 1}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{MbIds: []int32{1, 2}}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: -1, MbIds: []int32{1, 2}}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 0, MbIds: []int32{1, 2}}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 1, MbIds: []int32{}}, codes.InvalidArgument},
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 1, MbIds: []int32{0, -1}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 1, MbIds: []int32{1, 2}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteClassDescriptionForLocationIDExceptMBIDs(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClassDescriptionForLocationIDExceptMBIDs(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE class_description").WillReturnResult(sqlmock.NewResult(0, 2))

	response, err := server.DeleteClassDescriptionForLocationIDExceptMBIDs(context.Background(),
		&classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest{LocationId: 1, MbIds: []int32{12, 21, 12}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}
