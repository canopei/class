package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// MaxClassDescriptionsFetched is the limit of number of class descriptions that can be fetched
	// at once.
	MaxClassDescriptionsFetched int32 = 50
)

// CreateClassDescription creates a new ClassDescription
func (s *Server) CreateClassDescription(ctx context.Context, req *classPb.ClassDescription) (*classPb.ClassDescription, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClassDescription: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClassDescription")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The class description name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class description name is required.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		if req.SiteId < 1 {
			logger.Errorf("The site ID is required.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
		}

		rows, err := s.DB.Queryx(`SELECT class_description_id FROM class_description WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing class description.")
		}
		if rows.Next() {
			logger.Errorf("A class description already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO class_description
			(uuid, site_id, mb_id, name, description, class_level_id, session_type_id, prereq, notes, image_url, active)
		VALUES (unhex(replace(:uuid,'-','')), :site_id, :mb_id, :name, :description, :class_level_id, :session_type_id, :prereq,
			:notes, :image_url, :active)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	var classDescription = &classPb.ClassDescription{}
	err = s.DB.Get(classDescription, "SELECT * FROM class_description WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new class description: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new class description: %v", err)
		return nil, err
	}
	classDescription.Uuid = req.Uuid

	logger.Debugf("Created new class description '%s'.", classDescription.Uuid)

	return classDescription, nil
}

// GetClassDescription retrieves a ClassDescription
func (s *Server) GetClassDescription(ctx context.Context, req *classPb.GetClassDescriptionRequest) (*classPb.ClassDescription, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassDescription: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassDescription")

	// Validations
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" && req.Id < 1 {
		logger.Error("Either the class UUID or the ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Either the class UUID or the ID is required.")
	}
	if req.Uuid != "" && req.Id > 0 {
		logger.Error("Either the class UUID or the ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Either the class UUID or the ID is required.")
	}

	var classDescription classPb.ClassDescription
	err := s.DB.Get(&classDescription, `SELECT *
		FROM class_description
		WHERE
			(uuid = UNHEX(REPLACE(?,'-','')) OR class_description_id = ?)
			AND deleted_at = '0'`, req.Uuid, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class description UUID '%s' / ID %d.", req.Uuid, req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class description")
	}

	unpackedUUID, err := crypto.Parse([]byte(classDescription.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	classDescription.Uuid = unpackedUUID.String()

	return &classDescription, nil
}

// GetClassDescriptionByMBID retrieves a ClassDescription
func (s *Server) GetClassDescriptionByMBID(ctx context.Context, req *classPb.GetClassDescriptionByMBIDRequest) (*classPb.ClassDescription, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassDescriptionByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassDescriptionByMBID")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.MbId == 0 {
		logger.Error("The class description MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class description MB ID is required.")
	}

	var classDescription classPb.ClassDescription
	err := s.DB.Get(&classDescription, `SELECT *
		FROM class_description
		WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class description MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class description")
	}

	unpackedUUID, err := crypto.Parse([]byte(classDescription.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
	}
	classDescription.Uuid = unpackedUUID.String()

	return &classDescription, nil
}

// GetClassDescriptions fetches a list of class descriptions
func (s *Server) GetClassDescriptions(ctx context.Context, req *classPb.GetClassDescriptionsRequest) (*classPb.ClassDescriptionsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassDescriptions: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassDescriptions")

	// Validations
	if req.Limit == 0 || req.Limit > MaxClassDescriptionsFetched {
		req.Limit = 20
	}
	reqIds := map[string]bool{}
	for _, v := range req.Ids {
		uuid := strings.TrimSpace(v)
		if uuid != "" {
			reqIds[uuid] = true
		}
	}
	// Filtering by UUID was wanted, but the list is invalid
	if len(req.Ids) > 0 && len(reqIds) == 0 {
		logger.Error("The class descriptions UUIDs are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class descriptions UUIDs are required.")
	}

	logger.Debugf("Fetching %d class descriptions, offset %d, ids %#v...", req.Limit, req.Offset, reqIds)

	// Fetch the class descriptions
	queryParams := map[string]interface{}{
		"limit":  req.Limit,
		"offset": req.Offset,
	}

	condition := ""
	if len(reqIds) > 0 {
		condition = " AND uuid IN ("
		idx := 0
		for v := range req.Ids {
			paramKey := "id" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
			idx++
		}
		condition = strings.TrimSuffix(condition, ", ") + ")"
	}

	rows, err := s.DB.NamedQuery(`SELECT *
		FROM class_description
		WHERE deleted_at = '0' `+condition+`
		ORDER BY class_description_id
		DESC LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the class descriptions")
	}

	classDescriptionsList := &classPb.ClassDescriptionsList{}
	for rows.Next() {
		classDescription := classPb.ClassDescription{}
		err := rows.StructScan(&classDescription)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the class description")
		}

		unpackedUUID, err := crypto.Parse([]byte(classDescription.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for class description ID %d", classDescription.Id)
		}
		classDescription.Uuid = unpackedUUID.String()

		classDescriptionsList.ClassDescriptions = append(classDescriptionsList.ClassDescriptions, &classDescription)
	}

	return classDescriptionsList, nil
}

// UpdateClassDescription updates a ClassDescription
func (s *Server) UpdateClassDescription(ctx context.Context, req *classPb.ClassDescription) (*classPb.ClassDescription, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClassDescription: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClassDescription")

	// Validations
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The class description UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class description UUID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The class description name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class description name is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE class_description SET
			name = :name, description = :description, class_level_id = :class_level_id, session_type_id = :session_type_id, prereq = :prereq,
			notes = :notes, image_url = :image_url, active = :active
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var classDescription classPb.ClassDescription
	err = s.DB.Get(&classDescription, "SELECT * FROM class_description WHERE uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClassDescription - class description not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	classDescription.Uuid = req.Uuid

	return &classDescription, nil
}

// DeleteClassDescription removes a ClassDescription (sets deleted_at)
func (s *Server) DeleteClassDescription(ctx context.Context, req *classPb.DeleteClassDescriptionRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClassDescription: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClassDescription")

	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Error("The class description UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class description UUID is required.")
	}

	result, err := s.DB.Exec(`UPDATE class_description SET deleted_at = CURRENT_TIMESTAMP
		WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))`, req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClassDescription - class description not found for '%s'", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// DeleteClassDescriptionForLocationIDExceptMBIDs cleans the clients for a site ID except the given list
func (s *Server) DeleteClassDescriptionForLocationIDExceptMBIDs(ctx context.Context, req *classPb.DeleteClassDescriptionForLocationIDExceptMBIDsRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClassDescriptionForLocationIDExceptMBIDs: %v IDs", len(req.MbIds))
	defer timeTrack(logger, time.Now(), "DeleteClassDescriptionForLocationIDExceptMBIDs")

	// Validations
	if req.LocationId < 1 {
		logger.Error("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}

	MBIDs := map[int32]bool{}
	for _, v := range req.MbIds {
		if v > 0 {
			MBIDs[v] = true
		}
	}
	if len(MBIDs) == 0 {
		logger.Error("At least one class MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "At least one class MB ID is required.")
	}

	// Fetch the clients
	queryParams := map[string]interface{}{
		"location_id": req.LocationId,
	}
	condition := "AND cd.mb_id NOT IN ("
	idx := 0
	for v := range MBIDs {
		paramKey := "id" + strconv.Itoa(idx)
		condition += ":" + paramKey + ", "
		queryParams[paramKey] = v
		idx++
	}
	condition = strings.TrimSuffix(condition, ", ") + ")"

	_, err := s.DB.NamedExec(`UPDATE class_description cd
		INNER JOIN class_schedule cs ON cs.class_description_id = cd.class_description_id
		SET cd.deleted_at = CURRENT_TIMESTAMP
		WHERE cs.location_id = :location_id AND cd.deleted_at = '0' `+condition, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}

	return &empty.Empty{}, nil
}
