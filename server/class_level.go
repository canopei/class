package main

import (
	"database/sql"
	"time"

	"golang.org/x/net/context"

	"strings"

	classPb "bitbucket.org/canopei/class/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateClassLevel creates a new ClassLevel
func (s *Server) CreateClassLevel(ctx context.Context, req *classPb.ClassLevel) (*classPb.ClassLevel, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClassLevel: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClassLevel")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The class level name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class level name is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		rows, err := s.DB.Queryx(`SELECT class_level_id FROM class_level WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing class level.")
		}
		if rows.Next() {
			logger.Errorf("A class level already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	result, err := s.DB.NamedExec(`INSERT INTO class_level (site_id, mb_id, name, description)
		VALUES (:site_id, :mb_id, :name, :description)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	var classLevel = &classPb.ClassLevel{}
	err = s.DB.Get(classLevel, "SELECT * FROM class_level WHERE class_level_id = ?", lastInsertID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new class level: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new class level: %v", err)
		return nil, err
	}

	logger.Debugf("Created new class level %d.", classLevel.Id)

	return classLevel, nil
}

// GetClassLevel retrieves a ClassLevel
func (s *Server) GetClassLevel(ctx context.Context, req *classPb.ClassLevelRequest) (*classPb.ClassLevel, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassLevel: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassLevel")

	if req.Id < 1 {
		logger.Error("The class level ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class level ID is required.")
	}

	var classLevel classPb.ClassLevel
	err := s.DB.Get(&classLevel, `SELECT *
		FROM class_level
		WHERE class_level_id = ? AND deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class level ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class level")
	}

	return &classLevel, nil
}

// GetClassLevels fetches a list of class levels
func (s *Server) GetClassLevels(ctx context.Context, req *empty.Empty) (*classPb.ClassLevelsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassLevels: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassLevels")

	rows, err := s.DB.Queryx(`SELECT * FROM class_level WHERE deleted_at = '0'`)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the class levels")
	}

	classLevelsList := &classPb.ClassLevelsList{}
	for rows.Next() {
		classLevel := classPb.ClassLevel{}
		err := rows.StructScan(&classLevel)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a class level")
		}

		classLevelsList.ClassLevels = append(classLevelsList.ClassLevels, &classLevel)
	}

	return classLevelsList, nil
}

// GetClassLevelsBySiteID fetches a list of class levels for the given site ID
func (s *Server) GetClassLevelsBySiteID(ctx context.Context, req *classPb.GetClassLevelsBySiteIDRequest) (*classPb.ClassLevelsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassLevelsBySiteID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassLevelsBySiteID")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT * FROM class_level WHERE site_id = ? AND deleted_at = '0'`, req.SiteId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the class levels")
	}

	classLevelsList := &classPb.ClassLevelsList{}
	for rows.Next() {
		classLevel := classPb.ClassLevel{}
		err := rows.StructScan(&classLevel)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a class level")
		}

		classLevelsList.ClassLevels = append(classLevelsList.ClassLevels, &classLevel)
	}

	return classLevelsList, nil
}

// UpdateClassLevel updates a ClassLevel
func (s *Server) UpdateClassLevel(ctx context.Context, req *classPb.ClassLevel) (*classPb.ClassLevel, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClassLevel: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClassLevel")

	// Validations
	if req.Id < 1 {
		logger.Error("The class level ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class level ID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The class level name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class level name is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE class_level SET
			name = :name, description = :description
		WHERE deleted_at = '0' AND class_level_id = :class_level_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var classLevel classPb.ClassLevel
	err = s.DB.Get(&classLevel, "SELECT * FROM class_level WHERE class_level_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClassLevel - class level not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &classLevel, nil
}

// DeleteClassLevel removes a ClassLevel (sets deleted_at)
func (s *Server) DeleteClassLevel(ctx context.Context, req *classPb.ClassLevelRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClassLevel: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClassLevel")

	if req.Id < 1 {
		logger.Error("The class level ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class level ID is required.")
	}

	result, err := s.DB.Exec("UPDATE class_level SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND class_level_id = ?", req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClassLevel - class level not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
