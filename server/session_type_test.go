package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	sessionTypeColumns = []string{"session_type_id", "site_id", "mb_id", "program_id", "name", "default_time_length", "num_deducted", "created_at", "modified_at", "deleted_at"}
)

func TestCreateSessionTypeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.SessionType
		ExpectedCode codes.Code
		ExpectedTx   bool
	}{
		{&classPb.SessionType{}, codes.InvalidArgument, false},
		{&classPb.SessionType{Name: " ", ProgramId: 1}, codes.InvalidArgument, false},
		{&classPb.SessionType{Name: "foo", ProgramId: -1}, codes.InvalidArgument, false},
		{&classPb.SessionType{Name: "foo", ProgramId: 1, MbId: 1}, codes.InvalidArgument, false},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.SessionType{Name: "foo", ProgramId: 1, SiteId: 1}, codes.Unknown, true},
		{&classPb.SessionType{Name: "foo", ProgramId: 1, MbId: 1, SiteId: 1}, codes.Unknown, false},
	}
	for i, validationTest := range validationTests {
		if validationTest.ExpectedTx {
			mock.ExpectBegin()
		}
		sessionType, err := server.CreateSessionType(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(sessionType, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateSessionTypeSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(sqlmock.NewRows([]string{"session_type_id"}))
	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO session_type").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(sessionTypeColumns).AddRow(1, 1, 123, 1, "name", 60, 1, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(rows)
	mock.ExpectCommit()

	sessionType, err := server.CreateSessionType(context.Background(), &classPb.SessionType{
		Name:              "name",
		SiteId:            1,
		MbId:              123,
		ProgramId:         1,
		NumDeducted:       1,
		DefaultTimeLength: 60,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(sessionType)
	if sessionType != nil {
		assert.Equal("name", sessionType.Name)
	}
}

func TestCreateSessionTypeExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(sqlmock.NewRows([]string{"program_id"}).AddRow(1))

	sessionType, err := server.CreateSessionType(context.Background(), &classPb.SessionType{
		Name:              "name",
		SiteId:            1,
		MbId:              123,
		ProgramId:         1,
		NumDeducted:       1,
		DefaultTimeLength: 60,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(sessionType)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetSessionTypeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.SessionTypeRequest
		ExpectedCode codes.Code
	}{
		{&classPb.SessionTypeRequest{}, codes.InvalidArgument},
		{&classPb.SessionTypeRequest{Id: -2}, codes.InvalidArgument},
		{&classPb.SessionTypeRequest{Id: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.SessionTypeRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		sessionType, err := server.GetSessionType(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(sessionType, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSessionTypeSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(sessionTypeColumns).AddRow(1, 1, 123, 1, "name", 60, 1, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(rows)

	sessionType, err := server.GetSessionType(context.Background(), &classPb.SessionTypeRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(sessionType)
	if sessionType != nil {
		assert.Equal("name", sessionType.Name)
	}
}

func TestGetSessionTypeMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(sqlmock.NewRows(sessionTypeColumns))

	sessionType, err := server.GetSessionType(context.Background(), &classPb.SessionTypeRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(sessionType)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetSessionTypesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	sessionTypes := []*classPb.SessionType{
		&classPb.SessionType{Id: 1, SiteId: 1, MbId: 123, ProgramId: 1, Name: "foo", DefaultTimeLength: 60, NumDeducted: 1},
		&classPb.SessionType{Id: 2, SiteId: 2, MbId: 124, ProgramId: 1, Name: "bar", DefaultTimeLength: 90, NumDeducted: 2},
		&classPb.SessionType{Id: 3, SiteId: 1, MbId: 125, ProgramId: 2, Name: "foobar", DefaultTimeLength: 45, NumDeducted: 1},
	}
	stRows := sqlmock.NewRows(sessionTypeColumns)
	for _, pr := range sessionTypes {
		stRows.AddRow(pr.Id, pr.SiteId, pr.MbId, pr.ProgramId, pr.Name, pr.DefaultTimeLength, pr.NumDeducted, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(stRows)

	response, err := server.GetSessionTypes(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.SessionTypes, len(sessionTypes))
		// Sanity check
		assert.Equal(int32(90), response.SessionTypes[1].DefaultTimeLength)
	}
}

func TestGetSessionTypesEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(sqlmock.NewRows(sessionTypeColumns))

	response, err := server.GetSessionTypes(context.Background(), &empty.Empty{})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.SessionTypes, 0)
	}
}

func TestGetSessionTypesBySiteIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetSessionTypesBySiteIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetSessionTypesBySiteIDRequest{}, codes.InvalidArgument},
		{&classPb.GetSessionTypesBySiteIDRequest{SiteId: 0}, codes.InvalidArgument},
		{&classPb.GetSessionTypesBySiteIDRequest{SiteId: -2}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetSessionTypesBySiteIDRequest{SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetSessionTypesBySiteID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetSessionTypesBySiteIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	sessionTypes := []*classPb.SessionType{
		&classPb.SessionType{Id: 1, SiteId: 1, MbId: 123, ProgramId: 1, Name: "foo", DefaultTimeLength: 60, NumDeducted: 1},
		&classPb.SessionType{Id: 2, SiteId: 1, MbId: 124, ProgramId: 1, Name: "bar", DefaultTimeLength: 90, NumDeducted: 2},
		&classPb.SessionType{Id: 3, SiteId: 1, MbId: 125, ProgramId: 2, Name: "foobar", DefaultTimeLength: 45, NumDeducted: 1},
	}
	stRows := sqlmock.NewRows(sessionTypeColumns)
	for _, pr := range sessionTypes {
		stRows.AddRow(pr.Id, pr.SiteId, pr.MbId, pr.ProgramId, pr.Name, pr.DefaultTimeLength, pr.NumDeducted, "2017-01-01", "2017-01-02", "2017-01-03")
	}
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(stRows)

	response, err := server.GetSessionTypesBySiteID(context.Background(), &classPb.GetSessionTypesBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.SessionTypes, len(sessionTypes))
	}
}

func TestGetSessionTypesBySiteIDEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM session_type").WillReturnRows(sqlmock.NewRows(sessionTypeColumns))

	response, err := server.GetSessionTypesBySiteID(context.Background(), &classPb.GetSessionTypesBySiteIDRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.SessionTypes, 0)
	}
}

func TestUpdateSessionTypeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.SessionType
		ExpectedCode codes.Code
	}{
		{&classPb.SessionType{}, codes.InvalidArgument},
		{&classPb.SessionType{Id: 1, ProgramId: 1}, codes.InvalidArgument},
		{&classPb.SessionType{Name: "foo", ProgramId: 1}, codes.InvalidArgument},
		{&classPb.SessionType{Id: 1, Name: " ", ProgramId: 1}, codes.InvalidArgument},
		{&classPb.SessionType{Id: 0, Name: "foo", ProgramId: 1}, codes.InvalidArgument},
		{&classPb.SessionType{Id: 0, Name: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.SessionType{Id: 1, Name: "foo", ProgramId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		sessionType, err := server.UpdateSessionType(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(sessionType, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateSessionTypeMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE session_type SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WithArgs(1).WillReturnRows(sqlmock.NewRows(sessionTypeColumns))

	st := &classPb.SessionType{
		Id:                1,
		Name:              "foo",
		ProgramId:         1,
		DefaultTimeLength: 90,
		NumDeducted:       2,
	}

	sessionType, err := server.UpdateSessionType(context.Background(), st)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(sessionType)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateSessionTypeSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	st := &classPb.SessionType{
		Id:                1,
		Name:              "foo",
		ProgramId:         1,
		DefaultTimeLength: 90,
		NumDeducted:       2,
	}

	mock.ExpectExec("^UPDATE session_type SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(sessionTypeColumns).AddRow(1, 1, 123, 1, "name", 90, 2, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM session_type").WithArgs(st.Id).WillReturnRows(rows)

	sessionType, err := server.UpdateSessionType(context.Background(), st)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(sessionType)
	// sanity check
	if sessionType != nil {
		assert.Equal("name", sessionType.Name)
	}
}

func TestDeleteSessionTypeValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.SessionTypeRequest
		ExpectedCode codes.Code
	}{
		{&classPb.SessionTypeRequest{}, codes.InvalidArgument},
		{&classPb.SessionTypeRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.SessionTypeRequest{Id: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.SessionTypeRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteSessionType(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteSessionType(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE session_type SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteSessionType(context.Background(), &classPb.SessionTypeRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE session_type SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteSessionType(context.Background(), &classPb.SessionTypeRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
