package main

import (
	"database/sql"
	"strings"
	"time"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateSessionType creates a new SessionType
func (s *Server) CreateSessionType(ctx context.Context, req *classPb.SessionType) (*classPb.SessionType, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateSessionType: %v", req)
	defer timeTrack(logger, time.Now(), "CreateSessionType")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The session type name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The session type name is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.ProgramId < 1 {
		logger.Errorf("The program ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The program ID is required.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		rows, err := s.DB.Queryx(`SELECT session_type_id FROM session_type WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing session type.")
		}
		if rows.Next() {
			logger.Errorf("A session type already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	tx := s.DB.MustBegin()

	result, err := s.DB.NamedExec(`INSERT INTO session_type (site_id, mb_id, program_id, name, default_time_length, num_deducted)
		VALUES (:site_id, :mb_id, :program_id, :name, :default_time_length, :num_deducted)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	var sessionType = &classPb.SessionType{}
	err = s.DB.Get(sessionType, "SELECT * FROM session_type WHERE session_type_id = ?", lastInsertID)
	if err != nil {
		tx.Rollback()

		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new session type: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new session type: %v", err)
		return nil, err
	}

	logger.Debugf("Created new session type %d.", sessionType.Id)
	tx.Commit()

	return sessionType, nil
}

// GetSessionType retrieves a SessionType
func (s *Server) GetSessionType(ctx context.Context, req *classPb.SessionTypeRequest) (*classPb.SessionType, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSessionType: %v", req)
	defer timeTrack(logger, time.Now(), "GetSessionType")

	if req.Id < 1 {
		logger.Error("The session type ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The session type ID is required.")
	}

	var sessionType classPb.SessionType
	err := s.DB.Get(&sessionType, `SELECT
			st.*,
			p.program_id "program.program_id",
			p.mb_id "program.mb_id",
			p.name "program.name",
			p.schedule_type "program.schedule_type",
			p.cancel_offset "program.cancel_offset"
		FROM session_type st
		INNER JOIN program p on st.program_id = p.program_id
		WHERE st.session_type_id = ? AND st.deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find session type ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the session type")
	}

	return &sessionType, nil
}

// GetSessionTypes fetches a list of session types
func (s *Server) GetSessionTypes(ctx context.Context, req *empty.Empty) (*classPb.SessionTypesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSessionTypes: %v", req)
	defer timeTrack(logger, time.Now(), "GetSessionTypes")

	rows, err := s.DB.Queryx(`SELECT * FROM session_type WHERE deleted_at = '0'`)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the session types")
	}

	sessionTypesList := &classPb.SessionTypesList{}
	for rows.Next() {
		sessionType := classPb.SessionType{}
		err := rows.StructScan(&sessionType)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a session type")
		}

		sessionTypesList.SessionTypes = append(sessionTypesList.SessionTypes, &sessionType)
	}

	return sessionTypesList, nil
}

// GetSessionTypesBySiteID fetches a list of programs for the given site ID
func (s *Server) GetSessionTypesBySiteID(ctx context.Context, req *classPb.GetSessionTypesBySiteIDRequest) (*classPb.SessionTypesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetSessionTypesBySiteID: %v", req)
	defer timeTrack(logger, time.Now(), "GetSessionTypesBySiteID")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT * FROM session_type WHERE site_id = ? AND deleted_at = '0'`, req.SiteId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the session types")
	}

	sessionTypesList := &classPb.SessionTypesList{}
	for rows.Next() {
		sessionType := classPb.SessionType{}
		err := rows.StructScan(&sessionType)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a session type")
		}

		sessionTypesList.SessionTypes = append(sessionTypesList.SessionTypes, &sessionType)
	}

	return sessionTypesList, nil
}

// UpdateSessionType updates a SessionType
func (s *Server) UpdateSessionType(ctx context.Context, req *classPb.SessionType) (*classPb.SessionType, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateSessionType: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateSessionType")

	// Validations
	if req.Id < 1 {
		logger.Error("Invalid session type ID.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid session type ID.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The session type name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The session type name is required.")
	}
	if req.ProgramId < 1 {
		logger.Errorf("The program ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The program ID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE session_type SET
			program_id = :program_id, name = :name, default_time_length = :default_time_length, num_deducted = :num_deducted
		WHERE deleted_at = '0' AND session_type_id = :session_type_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var sessionType classPb.SessionType
	err = s.DB.Get(&sessionType, "SELECT * FROM session_type WHERE session_type_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateSessionType - session type not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &sessionType, nil
}

// DeleteSessionType removes a SessionType (sets deleted_at)
func (s *Server) DeleteSessionType(ctx context.Context, req *classPb.SessionTypeRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteSessionType: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteSessionType")

	if req.Id < 1 {
		logger.Error("The session type ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The session type ID is required.")
	}

	result, err := s.DB.Exec("UPDATE session_type SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND session_type_id = ?", req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteSessionType - session type not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
