package main

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateClassSchedule creates a new ClassSchedule
func (s *Server) CreateClassSchedule(ctx context.Context, req *classPb.ClassSchedule) (*classPb.ClassSchedule, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateClassSchedule: %v", req)
	defer timeTrack(logger, time.Now(), "CreateClassSchedule")

	// Validations
	if req.ClassDescriptionId < 1 {
		logger.Errorf("The description ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The description ID is required.")
	}
	if req.LocationId < 1 {
		logger.Errorf("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}
	if req.StaffId < 1 {
		logger.Errorf("The staff ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff ID is required.")
	}
	if req.StartDate == "" || req.EndDate == "" {
		logger.Errorf("The start and end date are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end date are required.")
	}
	if req.StartTime == "" || req.EndTime == "" {
		logger.Errorf("The start and end time are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end time are required.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		if req.SiteId < 1 {
			logger.Errorf("The site ID is required.")
			return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
		}

		rows, err := s.DB.Queryx(`SELECT class_schedule_id FROM class_schedule WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing class schedule.")
		}
		if rows.Next() {
			logger.Errorf("A class schedule already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	result, err := s.DB.NamedExec(`INSERT INTO class_schedule
			(site_id, mb_id, class_description_id, location_id, staff_id, is_available, day_sunday, day_monday, day_tuesday, day_wednesday,
			day_thursday, day_friday, day_saturday, allow_open_enrollment, allow_date_forward_enrollment, start_date, end_date,
			start_time, end_time)
		VALUES (:site_id, :mb_id, :class_description_id, :location_id, :staff_id, :is_available, :day_sunday, :day_monday, :day_tuesday,
			:day_wednesday, :day_thursday, :day_friday, :day_saturday, :allow_open_enrollment, :allow_date_forward_enrollment,
			:start_date, :end_date, :start_time, :end_time)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	var classSchedule = &classPb.ClassSchedule{}
	err = s.DB.Get(classSchedule, "SELECT * FROM class_schedule WHERE class_schedule_id = ?", lastInsertID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new class schedule: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new class schedule: %v", err)
		return nil, err
	}

	logger.Debugf("Created new class schedule ID %d.", classSchedule.Id)

	return classSchedule, nil
}

// GetClassSchedule retrieves a ClassSchedule
func (s *Server) GetClassSchedule(ctx context.Context, req *classPb.ClassScheduleRequest) (*classPb.ClassSchedule, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassSchedule: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassSchedule")

	if req.Id < 1 {
		logger.Error("The class schedule ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class schedule ID is required.")
	}

	var classSchedule classPb.ClassSchedule
	err := s.DB.Get(&classSchedule, `SELECT *
		FROM class_schedule
		WHERE class_schedule_id = ? AND deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class schedule ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class schedule")
	}

	return &classSchedule, nil
}

// GetClassSchedules retrieves multiple ClassSchedules
func (s *Server) GetClassSchedules(ctx context.Context, req *classPb.GetClassSchedulesRequest) (*classPb.ClassSchedulesList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassSchedules: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassSchedules")

	// Fetch the classes
	queryParams := map[string]interface{}{}

	condition := " AND class_schedule_id IN ("
	for idx, v := range req.Ids {
		paramKey := "id" + strconv.Itoa(idx)
		condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
		queryParams[paramKey] = v
	}
	condition = strings.TrimSuffix(condition, ", ") + ") "
	rows, err := s.DB.NamedQuery(`SELECT *
		FROM class_schedule
		WHERE deleted_at = '0' `+condition+`
		ORDER BY class_schedule_id ASC`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the classes")
	}

	classSchedulesList := &classPb.ClassSchedulesList{}
	for rows.Next() {
		classSchedule := classPb.ClassSchedule{}
		err := rows.StructScan(&classSchedule)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the class schedules")
		}

		classSchedulesList.ClassSchedules = append(classSchedulesList.ClassSchedules, &classSchedule)
	}

	logger.Debugf("Found %d class schedules out of %d.", len(classSchedulesList.ClassSchedules), len(req.Ids))

	return classSchedulesList, nil
}

// GetClassScheduleByMBID retrieves a ClassSchedule by MB ID
func (s *Server) GetClassScheduleByMBID(ctx context.Context, req *classPb.GetClassScheduleByMBIDRequest) (*classPb.ClassSchedule, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetClassScheduleByMBID: %v", req)
	defer timeTrack(logger, time.Now(), "GetClassScheduleByMBID")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	if req.MbId < 1 {
		logger.Error("The class schedule MB ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class schedule MB ID is required.")
	}

	var classSchedule classPb.ClassSchedule
	err := s.DB.Get(&classSchedule, `SELECT *
		FROM class_schedule
		WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find class schedule MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching class schedule")
	}

	return &classSchedule, nil
}

// UpdateClassSchedule updates a ClassSchedule
func (s *Server) UpdateClassSchedule(ctx context.Context, req *classPb.ClassSchedule) (*classPb.ClassSchedule, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateClassSchedule: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateClassSchedule")

	// validations
	if req.Id < 1 {
		logger.Error("The class schedule ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class schedule ID is required.")
	}
	if req.LocationId < 1 {
		logger.Errorf("The location ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The location ID is required.")
	}
	if req.StaffId < 1 {
		logger.Errorf("The staff ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff ID is required.")
	}
	if req.StartDate == "" || req.EndDate == "" {
		logger.Errorf("The start and end date are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end date are required.")
	}
	if req.StartTime == "" || req.EndTime == "" {
		logger.Errorf("The start and end time are required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The start and end time are required.")
	}

	_, err := s.DB.NamedExec(`UPDATE class_schedule SET
			is_available = :is_available, day_sunday = :day_sunday, day_monday = :day_monday, day_tuesday = :day_tuesday,
			day_wednesday = :day_wednesday, day_thursday = :day_thursday, day_friday = :day_friday, day_saturday = :day_saturday,
			allow_open_enrollment = :allow_open_enrollment, allow_date_forward_enrollment = :allow_date_forward_enrollment,
			start_time = :start_time, end_time = :end_time, start_date = :start_date, end_date = :end_date, staff_id = :staff_id,
			location_id = :location_id, class_description_id = :class_description_id
		WHERE deleted_at = '0' AND class_schedule_id = :class_schedule_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - create.")
	}

	var classSchedule classPb.ClassSchedule
	err = s.DB.Get(&classSchedule, "SELECT * FROM class_schedule WHERE class_schedule_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateClassSchedule - class schedule not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &classSchedule, nil
}

// DeleteClassSchedule removes a ClassSchedule (sets deleted_at)
func (s *Server) DeleteClassSchedule(ctx context.Context, req *classPb.ClassScheduleRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteClassSchedule: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteClassSchedule")

	if req.Id < 1 {
		logger.Error("The class schedule ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The class schedule ID is required.")
	}

	result, err := s.DB.Exec("UPDATE class_schedule SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND class_schedule_id = ?", req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteClassSchedule - class schedule not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetStaffLocationIDsByStaffIDs fetches the location IDs for the given list of staff IDs
func (s *Server) GetStaffLocationIDsByStaffIDs(ctx context.Context, req *classPb.GetStaffLocationIDsByStaffIDsRequest) (*classPb.GetStaffLocationIDsByStaffIDsResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffLocationIDsByStaffIDs: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffLocationIDsByStaffIDs")

	response := &classPb.GetStaffLocationIDsByStaffIDsResponse{}
	if len(req.StaffIds) == 0 {
		return response, nil
	}

	staffLocations := map[int32][]int32{}
	for _, staffID := range req.StaffIds {
		staffLocations[staffID] = []int32{}
	}

	// AND is_available = 1
	query, args, _ := sqlx.In(`SELECT location_id, staff_id
		FROM class_schedule
		WHERE staff_id IN (?) AND deleted_at = '0' AND (start_date > NOW() - INTERVAL 24 WEEK OR end_date > NOW())
		GROUP BY location_id, staff_id`, req.StaffIds)
	query = s.DB.Rebind(query)
	rows, err := s.DB.Queryx(query, args...)
	if err != nil {
		return nil, fmt.Errorf("Error while fetching the location phones")
	}
	for rows.Next() {
		var staffID int32
		var locationID int32

		err := rows.Scan(&locationID, &staffID)
		if err != nil {
			return nil, fmt.Errorf("Error while scanning the staff locations")
		}

		staffLocations[staffID] = append(staffLocations[staffID], locationID)
	}

	for staffID, locationIDs := range staffLocations {
		response.Staff = append(response.Staff, &classPb.StaffIdWithLocationIDs{
			StaffId:     staffID,
			LocationIds: locationIDs,
		})
	}

	return response, nil
}
