package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	bookingColumns = []string{"booking_id", "account_id", "class_id", "site_id", "location_id", "account_membership_id",
		"membership_service_id", "service_id", "status", "created_at", "modified_at"}
)

func TestCreateBookingValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Booking
		ExpectedCode codes.Code
	}{
		{&classPb.Booking{}, codes.InvalidArgument},
		{&classPb.Booking{ClassId: 2, SiteId: 3, LocationId: 4, ServiceId: 5}, codes.InvalidArgument},
		{&classPb.Booking{AccountId: 1, SiteId: 3, LocationId: 4, ServiceId: 5}, codes.InvalidArgument},
		{&classPb.Booking{AccountId: 1, ClassId: 2, LocationId: 4, ServiceId: 5}, codes.InvalidArgument},
		{&classPb.Booking{AccountId: 1, ClassId: 2, SiteId: 3, ServiceId: 5}, codes.InvalidArgument},
		{&classPb.Booking{AccountId: 1, ClassId: 2, SiteId: 3, LocationId: 4}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.Booking{AccountId: 1, ClassId: 2, SiteId: 3, LocationId: 4, AccountMembershipId: 5, MembershipServiceId: 6}, codes.Unknown},
		{&classPb.Booking{AccountId: 1, ClassId: 2, SiteId: 3, LocationId: 4, AccountMembershipId: 5}, codes.Unknown},
		{&classPb.Booking{AccountId: 1, ClassId: 2, SiteId: 3, LocationId: 4, ServiceId: 5}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		booking, err := server.CreateBooking(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(booking, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateBookingSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT INTO booking").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(bookingColumns).
		AddRow(1, 2, 3, 4, 5, 6, 7, 0, "active", "2017-01-01", "2017-01-02")
	mock.ExpectQuery("^SELECT (.+) FROM booking").WillReturnRows(rows)

	booking, err := server.CreateBooking(context.Background(), &classPb.Booking{
		AccountId:           1,
		ClassId:             2,
		SiteId:              3,
		LocationId:          4,
		AccountMembershipId: 5,
		MembershipServiceId: 7,
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(booking)
	if booking != nil {
		assert.Equal(int32(7), booking.MembershipServiceId)
	}
}

func TestUpdateBookingMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE booking SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM booking WHERE").WillReturnRows(sqlmock.NewRows(bookingColumns))

	booking, err := server.UpdateBooking(context.Background(), &classPb.Booking{Id: 1, Status: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(booking)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateBookingSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testBooking := &classPb.Booking{
		Id:                  1,
		AccountId:           1,
		ClassId:             2,
		SiteId:              3,
		LocationId:          4,
		AccountMembershipId: 5,
		MembershipServiceId: 7,
	}

	mock.ExpectExec("^UPDATE booking SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(bookingColumns).AddRow(
		testBooking.Id, testBooking.AccountId, testBooking.ClassId, testBooking.SiteId, testBooking.LocationId,
		testBooking.AccountMembershipId, testBooking.MembershipServiceId, 0, "active", "2017-01-01", "2017-01-02")
	mock.ExpectQuery("^SELECT (.+) FROM booking WHERE").WillReturnRows(rows)

	booking, err := server.UpdateBooking(context.Background(), testBooking)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(booking)
	// sanity check
	if booking != nil {
		assert.Equal(int32(7), booking.MembershipServiceId)
	}
}

func TestGetBookingForAccountAndClassValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Booking
		ExpectedCode codes.Code
	}{
		{&classPb.Booking{}, codes.InvalidArgument},
		{&classPb.Booking{AccountId: 1}, codes.InvalidArgument},
		{&classPb.Booking{ClassId: 2}, codes.InvalidArgument},

		{&classPb.Booking{AccountId: 1, ClassId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		booking, err := server.GetBookingForAccountAndClass(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(booking, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetBookingForAccountAndClassMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM booking").WillReturnRows(sqlmock.NewRows(bookingColumns))

	booking, err := server.GetBookingForAccountAndClass(context.Background(), &classPb.Booking{AccountId: 1, ClassId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(booking)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetBookingForAccountAndClassSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(bookingColumns).AddRow(1, 2, 3, 4, 5, 6, 7, 0, "active", "2017-01-01", "2017-01-02")
	mock.ExpectQuery("^SELECT (.+) FROM booking").WillReturnRows(rows)

	booking, err := server.GetBookingForAccountAndClass(context.Background(), &classPb.Booking{AccountId: 1, ClassId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(booking)
	if booking != nil {
		assert.Equal(int32(7), booking.MembershipServiceId)
	}
}
