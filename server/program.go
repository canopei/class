package main

import (
	"database/sql"
	"strings"
	"time"

	"golang.org/x/net/context"

	classPb "bitbucket.org/canopei/class/protobuf"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// CreateProgram creates a new Program
func (s *Server) CreateProgram(ctx context.Context, req *classPb.Program) (*classPb.Program, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateProgram: %v", req)
	defer timeTrack(logger, time.Now(), "CreateProgram")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The programn name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The program name is required.")
	}
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	// If we got a MB ID, check for duplicate
	if req.MbId != 0 {
		rows, err := s.DB.Queryx(`SELECT program_id FROM program WHERE mb_id = ? AND site_id = ? AND deleted_at = '0'`, req.MbId, req.SiteId)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing program.")
		}
		if rows.Next() {
			logger.Errorf("A program already exists with this MB ID %d.", req.MbId)
			return nil, grpc.Errorf(codes.AlreadyExists, "")
		}
	}

	result, err := s.DB.NamedExec(`INSERT INTO program (site_id, mb_id, name, schedule_type, cancel_offset, dropin_service_mb_id)
		VALUES (:site_id, :mb_id, :name, :schedule_type, :cancel_offset, :dropin_service_mb_id)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot get LAST_INSERT_ID.")
	}

	var program = &classPb.Program{}
	err = s.DB.Get(program, "SELECT * FROM program WHERE program_id = ?", lastInsertID)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find the new program: %v", err)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		logger.Errorf("Error while fetching the new program: %v", err)
		return nil, err
	}

	logger.Debugf("Created new program %d.", program.Id)

	return program, nil
}

// GetProgram retrieves a Program
func (s *Server) GetProgram(ctx context.Context, req *classPb.ProgramRequest) (*classPb.Program, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetProgram: %v", req)
	defer timeTrack(logger, time.Now(), "GetProgram")

	if req.Id < 1 {
		logger.Error("The programn ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The programn ID is required.")
	}

	var program classPb.Program
	err := s.DB.Get(&program, `SELECT *
		FROM program
		WHERE program_id = ? AND deleted_at = '0'`, req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find program ID %d.", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the program")
	}

	return &program, nil
}

// GetPrograms fetches a list of session types
func (s *Server) GetPrograms(ctx context.Context, req *empty.Empty) (*classPb.ProgramsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetPrograms: %v", req)
	defer timeTrack(logger, time.Now(), "GetPrograms")

	rows, err := s.DB.Queryx(`SELECT * FROM program WHERE deleted_at = '0'`)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the programs")
	}

	programsList := &classPb.ProgramsList{}
	for rows.Next() {
		program := classPb.Program{}
		err := rows.StructScan(&program)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a program")
		}

		programsList.Programs = append(programsList.Programs, &program)
	}

	return programsList, nil
}

// GetProgramsBySiteID fetches a list of programs for the given site ID
func (s *Server) GetProgramsBySiteID(ctx context.Context, req *classPb.GetProgramsBySiteIDRequest) (*classPb.ProgramsList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetProgramsBySiteID: %v", req)
	defer timeTrack(logger, time.Now(), "GetProgramsBySiteID")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT * FROM program WHERE site_id = ? AND deleted_at = '0'`, req.SiteId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the programs")
	}

	programsList := &classPb.ProgramsList{}
	for rows.Next() {
		program := classPb.Program{}
		err := rows.StructScan(&program)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a program")
		}

		programsList.Programs = append(programsList.Programs, &program)
	}

	return programsList, nil
}

// UpdateProgram updates a Program
func (s *Server) UpdateProgram(ctx context.Context, req *classPb.Program) (*classPb.Program, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateProgram: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateProgram")

	// Validations
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Errorf("The programn name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The program name is required.")
	}
	if req.Id < 1 {
		logger.Error("The programn ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The programn ID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE program SET
			name = :name, schedule_type = :schedule_type, cancel_offset = :cancel_offset, dropin_service_mb_id = :dropin_service_mb_id
		WHERE deleted_at = '0' AND program_id = :program_id`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var program classPb.Program
	err = s.DB.Get(&program, "SELECT * FROM program WHERE program_id = ?", req.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateProgram - program not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}

	return &program, nil
}

// DeleteProgram removes a Program (sets deleted_at)
func (s *Server) DeleteProgram(ctx context.Context, req *classPb.ProgramRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteProgram: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteProgram")

	if req.Id < 1 {
		logger.Error("Invalid program ID.")
		return nil, grpc.Errorf(codes.InvalidArgument, "Invalid program ID.")
	}

	result, err := s.DB.Exec("UPDATE program SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND program_id = ?", req.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteProgram - program not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
