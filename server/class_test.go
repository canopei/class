package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	classColumns = []string{"class_id", "uuid", "site_id", "mb_id", "class_schedule_id", "staff_id", "max_capacity", "web_capacity", "total_booked",
		"total_booked_waitlist", "web_booked", "is_canceled", "substitute", "active", "is_available", "is_waitlist_available",
		"is_enrolled", "hide_cancel", "start_datetime", "end_datetime", "created_at", "modified_at", "deleted_at"}

	completeClassColumns = []string{"class_id", "uuid", "site_id", "mb_id", "class_schedule_id", "staff_id", "max_capacity", "web_capacity", "total_booked",
		"total_booked_waitlist", "web_booked", "is_canceled", "substitute", "active", "is_available", "is_waitlist_available",
		"is_enrolled", "hide_cancel", "start_datetime", "end_datetime", "created_at", "modified_at", "deleted_at",
		"location_id", "session_type_id", "session_type_name", "class_description.uuid", "class_description.name",
		"class_description.description", "class_description.prereq", "class_description.notes", "class_description.class_level_id",
		"class_level.class_level_id", "class_level.name", "class_level.description"}
)

func TestCreateClassValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Class
		ExpectedCode codes.Code
	}{
		{&classPb.Class{}, codes.InvalidArgument},
		{&classPb.Class{StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, EndDatetime: "2017-02-01 00:00:00"}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00"}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00", MbId: -2, SiteId: 1}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 28:00:00", MbId: 1, SiteId: 1}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-13-01 00:00:00", EndDatetime: "2017-02-01 00:00:00", MbId: 1, SiteId: 1}, codes.InvalidArgument},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00", MbId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}, codes.Unknown},
		{&classPb.Class{ClassScheduleId: 1, StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00", MbId: 1, SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		class, err := server.CreateClass(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(class, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClassSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows([]string{"program_id"}))
	mock.ExpectExec("^INSERT INTO class").WillReturnResult(sqlmock.NewResult(1, 1))

	rows := sqlmock.NewRows(classColumns).AddRow(
		1, "1234567890123456", 1, 123, 1, 6, 20, 2, 10, 12, 2, false, false, true, true, true, true, false, "2017-01-01 00:00:00",
		"2017-02-01 00:00:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(rows)

	class, err := server.CreateClass(context.Background(), &classPb.Class{
		SiteId:          1,
		MbId:            123,
		ClassScheduleId: 1,
		WebBooked:       3,
		MaxCapacity:     20,
		StartDatetime:   "2017-01-01 00:00:00",
		EndDatetime:     "2017-02-01 00:00:00",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(class)
	if class != nil {
		assert.Equal("2017-01-01 00:00:00", class.StartDatetime)
	}
}

func TestCreateClassExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows([]string{"class_id"}).AddRow(1))

	class, err := server.CreateClass(context.Background(), &classPb.Class{
		SiteId:          1,
		MbId:            123,
		ClassScheduleId: 1,
		WebBooked:       3,
		MaxCapacity:     20,
		StartDatetime:   "2017-01-01 00:00:00",
		EndDatetime:     "2017-02-01 00:00:00",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(class)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetClassByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassByMBIDRequest{}, codes.InvalidArgument},
		{&classPb.GetClassByMBIDRequest{MbId: -2}, codes.InvalidArgument},
		{&classPb.GetClassByMBIDRequest{MbId: 0}, codes.InvalidArgument},
		{&classPb.GetClassByMBIDRequest{MbId: 1}, codes.InvalidArgument},
		{&classPb.GetClassByMBIDRequest{SiteId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassByMBIDRequest{MbId: 1, SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		class, err := server.GetClassByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(class, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classColumns).AddRow(
		1, "1234567890123456", 1, 123, 1, 6, 20, 10, 12, 2, 4, false, false, true, true, true, true, false, "2017-01-01 00:00",
		"2017-02-01 00:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(rows)

	class, err := server.GetClassByMBID(context.Background(), &classPb.GetClassByMBIDRequest{MbId: 123, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(class)
	if class != nil {
		assert.Equal(int32(20), class.MaxCapacity)
	}
}

func TestGetClassByMBIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows(classColumns))

	class, err := server.GetClassByMBID(context.Background(), &classPb.GetClassByMBIDRequest{MbId: 123, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(class)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	classes := []*classPb.Class{
		&classPb.Class{Id: 1, Uuid: "1234567890123456", MbId: 123, ClassScheduleId: 1, MaxCapacity: 10},
		&classPb.Class{Id: 2, Uuid: "1234567890123457", MbId: 124, ClassScheduleId: 2, MaxCapacity: 20},
		&classPb.Class{Id: 3, Uuid: "1234567890123458", MbId: 125, ClassScheduleId: 3, MaxCapacity: 15},
	}

	classesRows := sqlmock.NewRows(completeClassColumns)
	for _, cl := range classes {
		classesRows.AddRow(
			cl.Id, cl.Uuid, 1, cl.MbId, cl.ClassScheduleId, 6, cl.MaxCapacity, 10, 12, 2, 4, false, false, true, true, true, true, false, "2017-01-01 00:00",
			"2017-02-01 00:00", "2017-01-01", "2017-01-02", "2017-01-03",
			1, 3, "stName", "1234567890123456", "cdName", "cdDesc", "cdPrereq", "cdNotes", 4, 4, "clName", "clDesc",
		)
	}
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(classesRows)

	response, err := server.GetClasses(context.Background(), &classPb.GetClassesRequest{Offset: 10, Limit: 15, Uuids: "uuid1,uuid2, "})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Classes, len(classes))

		// Sanity checks
		assert.Equal(int32(20), response.Classes[1].MaxCapacity)
		assert.Equal("cdName", response.Classes[1].Description.Name)
		assert.Equal("clName", response.Classes[1].Level.Name)
		assert.Equal(int32(3), response.Classes[1].SessionTypeId)
		assert.Equal("stName", response.Classes[1].SessionTypeName)
	}
}

func TestGetClassesSuccessfulSingle(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	classes := []*classPb.Class{
		&classPb.Class{Id: 1, Uuid: "1234567890123456", MbId: 123, ClassScheduleId: 1, MaxCapacity: 10},
	}

	classesRows := sqlmock.NewRows(completeClassColumns)
	for _, cl := range classes {
		classesRows.AddRow(
			cl.Id, cl.Uuid, 1, cl.MbId, cl.ClassScheduleId, 6, cl.MaxCapacity, 10, 12, 2, 4, false, false, true, true, true, true, false, "2017-01-01 00:00",
			"2017-02-01 00:00", "2017-01-01", "2017-01-02", "2017-01-03",
			1, 3, "stName", "1234567890123456", "cdName", "cdDesc", "cdPrereq", "cdNotes", 4, 4, "clName", "clDesc",
		)
	}
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(classesRows)

	response, err := server.GetClasses(context.Background(), &classPb.GetClassesRequest{Offset: 10, Limit: 15, Uuids: "uuid1"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Classes, len(classes))

		// Sanity checks
		assert.Equal(int32(10), response.Classes[0].MaxCapacity)
		assert.Equal("cdName", response.Classes[0].Description.Name)
		assert.Equal("clName", response.Classes[0].Level.Name)
		assert.Equal(int32(3), response.Classes[0].SessionTypeId)
		assert.Equal("stName", response.Classes[0].SessionTypeName)
	}
}

func TestGetClassesEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows(completeClassColumns))

	response, err := server.GetClasses(context.Background(), &classPb.GetClassesRequest{Offset: 10, Limit: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Classes, 0)
	}
}

func TestGetClassesByLocationIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassesByLocationIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassesByLocationIDRequest{}, codes.InvalidArgument},
		{&classPb.GetClassesByLocationIDRequest{From: "2017-01-01 00:00:00", To: "2017-01-01 23:59:00"}, codes.InvalidArgument},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-14-01 00:00:00", To: "2017-01-01 23:59:00"}, codes.InvalidArgument},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-14-01 00:00:00", To: "2017-01-35 23:59:00"}, codes.InvalidArgument},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-01-01 00:00:00", To: "2017-01-01 27:59:00"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1}, codes.Unknown},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, To: "2017-01-01 23:59:00"}, codes.Unknown},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-01-01 00:00:00"}, codes.Unknown},
		{&classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-01-01 00:00:00", To: "2017-01-01 23:59:00"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetClassesByLocationID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassesByLocationIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	classes := []*classPb.Class{
		&classPb.Class{Id: 1, Uuid: "1234567890123456", MbId: 123, ClassScheduleId: 1, MaxCapacity: 10},
		&classPb.Class{Id: 2, Uuid: "1234567890123457", MbId: 124, ClassScheduleId: 2, MaxCapacity: 20},
		&classPb.Class{Id: 3, Uuid: "1234567890123458", MbId: 125, ClassScheduleId: 3, MaxCapacity: 15},
	}

	classesRows := sqlmock.NewRows(completeClassColumns)
	for _, cl := range classes {
		classesRows.AddRow(
			cl.Id, cl.Uuid, 1, cl.MbId, cl.ClassScheduleId, 6, cl.MaxCapacity, 10, 12, 2, 4, false, false, true, true, true, true, false, "2017-01-01 00:00",
			"2017-02-01 00:00", "2017-01-01", "2017-01-02", "2017-01-03",
			1, 3, "stName", "1234567890123456", "cdName", "cdDesc", "cdPrereq", "cdNotes", 4, 4, "clName", "clDesc",
		)
	}
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(classesRows)

	response, err := server.GetClassesByLocationID(context.Background(), &classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-01-01 00:00:00", To: "2017-01-01 23:59:00"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Classes, len(classes))
		// Sanity check
		assert.Equal(int32(20), response.Classes[1].MaxCapacity)
		assert.Equal(int32(3), response.Classes[2].ClassScheduleId)
	}
}

func TestGetClassesByLocationIDEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows(completeClassColumns))

	response, err := server.GetClassesByLocationID(context.Background(), &classPb.GetClassesByLocationIDRequest{LocationId: 1, From: "2017-01-01 00:00:00", To: "2017-01-01 23:59:00"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Classes, 0)
	}
}

func TestUpdateClassValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.Class
		ExpectedCode codes.Code
	}{
		{&classPb.Class{}, codes.InvalidArgument},
		{&classPb.Class{StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}, codes.InvalidArgument},
		{&classPb.Class{Uuid: "12345678901234576", EndDatetime: "2017-02-01 00:00:00", MbId: 1}, codes.InvalidArgument},
		{&classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-01-01 00:00:00", MbId: 1}, codes.InvalidArgument},
		{&classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-23-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}, codes.InvalidArgument},
		{&classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 27:00:00"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		class, err := server.UpdateClass(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(class, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClassMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE class SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM class").WillReturnRows(sqlmock.NewRows(classColumns))

	class, err := server.UpdateClass(context.Background(), &classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(class)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClassSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClass := &classPb.Class{Uuid: "12345678901234576", StartDatetime: "2017-01-01 00:00:00", EndDatetime: "2017-02-01 00:00:00"}

	mock.ExpectExec("^UPDATE class SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(classColumns).AddRow(
		1, "1234567890123456", 1, 123, 1, 6, 20, 10, 12, 2, 4, false, false, true, true, true, true, false, "2017-01-01 00:00:00",
		"2017-02-01 00:00:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class WHERE").WithArgs(testClass.Uuid).WillReturnRows(rows)

	class, err := server.UpdateClass(context.Background(), testClass)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(class)
	// sanity check
	if class != nil {
		assert.Equal(int32(20), class.MaxCapacity)
	}
}

func TestDeleteClassValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.DeleteClassRequest
		ExpectedCode codes.Code
	}{
		{&classPb.DeleteClassRequest{}, codes.InvalidArgument},
		{&classPb.DeleteClassRequest{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.DeleteClassRequest{Uuid: "uuid"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteClass(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClass(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE class SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClass(context.Background(), &classPb.DeleteClassRequest{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE class SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteClass(context.Background(), &classPb.DeleteClassRequest{Uuid: "uuid"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
