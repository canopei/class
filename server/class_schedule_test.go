package main

import (
	"context"
	"testing"

	classPb "bitbucket.org/canopei/class/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	classScheduleColumns = []string{"class_schedule_id", "site_id", "mb_id", "class_description_id", "location_id", "staff_id",
		"is_available", "day_sunday", "day_monday", "day_tuesday", "day_wednesday", "day_thursday", "day_friday", "day_saturday",
		"allow_open_enrollment", "allow_date_forward_enrollment", "start_date", "end_date", "start_time", "end_time",
		"created_at", "modified_at", "deleted_at"}
)

func TestCreateClassScheduleValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassSchedule
		ExpectedCode codes.Code
	}{
		{&classPb.ClassSchedule{}, codes.InvalidArgument},
		{&classPb.ClassSchedule{StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, LocationId: 1,
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, MbId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.Unknown},
		{&classPb.ClassSchedule{ClassDescriptionId: 1, StaffId: 1, MbId: 1, SiteId: 1, LocationId: 1, StartDate: "2017-01-01",
			EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classSch, err := server.CreateClassSchedule(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classSch, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateClassScheduleSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(sqlmock.NewRows([]string{"class_schedule_id"}))
	mock.ExpectExec("^INSERT INTO class_schedule").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(classScheduleColumns).AddRow(
		1, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(rows)

	classSch, err := server.CreateClassSchedule(context.Background(), &classPb.ClassSchedule{
		MbId: 123, SiteId: 1, ClassDescriptionId: 1, StaffId: 1, LocationId: 1, DayWednesday: true, IsAvailable: true, StartDate: "2017-01-01",
		EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classSch)
	if classSch != nil {
		assert.Equal(int32(1), classSch.ClassDescriptionId)
		assert.Equal(true, classSch.DayWednesday)
		assert.Equal("2017-01-01", classSch.StartDate)
	}
}

func TestCreateClassScheduleExisting(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(sqlmock.NewRows([]string{"class_schedule_id"}).AddRow(1))

	classSch, err := server.CreateClassSchedule(context.Background(), &classPb.ClassSchedule{
		MbId: 123, SiteId: 1, ClassDescriptionId: 1, StaffId: 1, LocationId: 1, DayWednesday: true, IsAvailable: true, StartDate: "2017-01-01",
		EndDate: "2017-01-02", StartTime: "12:00", EndTime: "13:00",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classSch)
	assert.NotNil(err)
	assert.Equal(codes.AlreadyExists, grpc.Code(err))
}

func TestGetClassScheduleValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassScheduleRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ClassScheduleRequest{}, codes.InvalidArgument},
		{&classPb.ClassScheduleRequest{Id: -2}, codes.InvalidArgument},
		{&classPb.ClassScheduleRequest{Id: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassScheduleRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classSch, err := server.GetClassSchedule(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classSch, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassScheduleSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classScheduleColumns).AddRow(
		1, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class_schedule WHERE").WillReturnRows(rows)

	classSch, err := server.GetClassSchedule(context.Background(), &classPb.ClassScheduleRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classSch)
	if classSch != nil {
		assert.Equal(true, classSch.DayWednesday)
	}
}

func TestGetClassScheduleMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(sqlmock.NewRows(classScheduleColumns))

	classSch, err := server.GetClassSchedule(context.Background(), &classPb.ClassScheduleRequest{Id: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classSch)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassScheduleByMBIDValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.GetClassScheduleByMBIDRequest
		ExpectedCode codes.Code
	}{
		{&classPb.GetClassScheduleByMBIDRequest{}, codes.InvalidArgument},
		{&classPb.GetClassScheduleByMBIDRequest{MbId: 1}, codes.InvalidArgument},
		{&classPb.GetClassScheduleByMBIDRequest{MbId: -2, SiteId: 1}, codes.InvalidArgument},
		{&classPb.GetClassScheduleByMBIDRequest{MbId: 0, SiteId: 1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.GetClassScheduleByMBIDRequest{MbId: 1, SiteId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classSch, err := server.GetClassScheduleByMBID(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classSch, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetClassScheduleByMBIDSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classScheduleColumns).AddRow(
		1, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class_schedule WHERE").WillReturnRows(rows)

	classSch, err := server.GetClassScheduleByMBID(context.Background(), &classPb.GetClassScheduleByMBIDRequest{MbId: 123, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classSch)
	if classSch != nil {
		assert.Equal(true, classSch.DayWednesday)
	}
}

func TestGetClassScheduleByMBIDMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(sqlmock.NewRows(classScheduleColumns))

	classSch, err := server.GetClassScheduleByMBID(context.Background(), &classPb.GetClassScheduleByMBIDRequest{MbId: 123, SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classSch)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestGetClassSchedulesMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(sqlmock.NewRows(classScheduleColumns))

	classSch, err := server.GetClassSchedules(context.Background(), &classPb.GetClassSchedulesRequest{Ids: []int32{1, 2}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(classSch)
	assert.Nil(err)
	assert.Equal(0, len(classSch.ClassSchedules))
}

func TestGetClassSchedulesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(classScheduleColumns)
	rows.AddRow(
		1, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	rows.AddRow(
		2, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)

	mock.ExpectQuery("^SELECT (.+) FROM class_schedule").WillReturnRows(rows)

	classSch, err := server.GetClassSchedules(context.Background(), &classPb.GetClassSchedulesRequest{Ids: []int32{1, 2}})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(classSch)
	assert.Nil(err)
	assert.Equal(2, len(classSch.ClassSchedules))
}

func TestUpdateClassScheduleValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassSchedule
		ExpectedCode codes.Code
	}{
		{&classPb.ClassSchedule{}, codes.InvalidArgument},
		{&classPb.ClassSchedule{StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, LocationId: 1, EndDate: "2017-01-02",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01",
			StartTime: "12:00", EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			EndTime: "13:00"}, codes.InvalidArgument},
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			StartTime: "12:00"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassSchedule{Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
			StartTime: "12:00", EndTime: "13:00"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		classSch, err := server.UpdateClassSchedule(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(classSch, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateClassScheduleMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClassSchedule := &classPb.ClassSchedule{
		Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
		StartTime: "12:00", EndTime: "13:00",
	}

	mock.ExpectExec("^UPDATE class_schedule SET").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM class_schedule WHERE").WithArgs(1).WillReturnRows(sqlmock.NewRows(classScheduleColumns))

	classSch, err := server.UpdateClassSchedule(context.Background(), testClassSchedule)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(classSch)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateClassScheduleSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testClassSchedule := &classPb.ClassSchedule{
		Id: 1, StaffId: 1, LocationId: 1, StartDate: "2017-01-01", EndDate: "2017-01-02",
		StartTime: "12:00", EndTime: "13:00",
	}

	mock.ExpectExec("^UPDATE class_schedule SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(classScheduleColumns).AddRow(
		1, 1, 123, 1, 1, 1, true, false, false, false, true, false, false, false, true, true, "2017-01-01", "2017-01-02",
		"12:00", "13:00", "2017-01-01", "2017-01-02", "2017-01-03",
	)
	mock.ExpectQuery("^SELECT (.+) FROM class_schedule WHERE").WithArgs(testClassSchedule.Id).WillReturnRows(rows)

	classSch, err := server.UpdateClassSchedule(context.Background(), testClassSchedule)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(classSch)
	// sanity check
	if classSch != nil {
		assert.Equal(true, classSch.DayWednesday)
		assert.Equal("2017-01-01", classSch.StartDate)
	}
}

func TestDeleteClassScheduleValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *classPb.ClassScheduleRequest
		ExpectedCode codes.Code
	}{
		{&classPb.ClassScheduleRequest{}, codes.InvalidArgument},
		{&classPb.ClassScheduleRequest{Id: 0}, codes.InvalidArgument},
		{&classPb.ClassScheduleRequest{Id: -1}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&classPb.ClassScheduleRequest{Id: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteClassSchedule(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteClassSchedule(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE class_schedule(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteClassSchedule(context.Background(), &classPb.ClassScheduleRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE class_schedule(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteClassSchedule(context.Background(), &classPb.ClassScheduleRequest{Id: 1})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
