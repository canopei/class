package main

import (
	"golang.org/x/net/context"

	"bitbucket.org/canopei/class/config"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// Server is the class service implementation
type Server struct {
	Config        *config.Config
	Logger        *logrus.Entry
	DB            *sqlx.DB
	ClassMBClient *classMb.Class_x0020_ServiceSoap
	Queue         *nsq.Producer
}

// NewServer creates a new Server instance
func NewServer(
	logger *logrus.Entry,
	db *sqlx.DB,
	classMBClient *classMb.Class_x0020_ServiceSoap,
	queue *nsq.Producer,
) *Server {
	return &Server{
		Logger:        logger,
		DB:            db,
		ClassMBClient: classMBClient,
		Queue:         queue,
	}
}

// Ping is used for healthchecks
func (s *Server) Ping(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
