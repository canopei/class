FROM alpine:latest
MAINTAINER Aris Buzachis <aris@canopei.com>

RUN apk add --update --no-cache ca-certificates

RUN mkdir -p /var/sng/bin
COPY build/ /var/sng/bin/
RUN chmod +x /var/sng/bin/server \
    && chmod +x /var/sng/bin/api \
    && chmod +x /var/sng/bin/sync

EXPOSE 8080
EXPOSE 8180

CMD cd /var/sng/bin; ./server & ./api & ./sync & ./reindex & tail -f /dev/null