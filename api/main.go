package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc"

	"bitbucket.org/canopei/account"
	"bitbucket.org/canopei/class"
	"bitbucket.org/canopei/class/config"
	cApi "bitbucket.org/canopei/golibs/api"
	"bitbucket.org/canopei/golibs/elasticsearch"
	cGrpc "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/healthcheck"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/staff"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/gorilla/mux"
	nsq "github.com/nsqio/go-nsq"

	"fmt"

	accountPb "bitbucket.org/canopei/account/protobuf"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/logging"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	"bitbucket.org/canopei/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/handlers"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/urfave/negroni"
)

var (
	conf           *config.Config
	logger         *logrus.Entry
	siteService    sitePb.SiteServiceClient
	classService   classPb.ClassServiceClient
	staffService   staffPb.StaffServiceClient
	clientService  accountPb.ClientServiceClient
	accountService accountPb.AccountServiceClient
	syncService    *class.SyncService

	version string
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := mux.NewRouter()
	mux.HandleFunc(healthcheck.Healthpath, HealthcheckHandler)

	classServer := NewClassServer(logger, siteService, classService, staffService, clientService, accountService, syncService, &conf.Mindbody)
	classServer.RegisterRoutes(mux)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONBuiltin{}))
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBackoffMaxDelay(8 * time.Second)}

	err := classPb.RegisterClassServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)

	if err != nil {
		return err
	}

	mux.PathPrefix("/").Handler(cGrpc.RestGatewayResponseInterceptor(gwmux))

	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(cApi.NewServiceAPIMiddleware(logger))
	n.Use(cApi.NewCombinedLoggingMiddleware(logger, mux, handlers.CombinedLoggingHandler))
	n.Use(negroni.NewRecovery())

	logger.Infof("Serving on %d.", conf.Service.ApiPort)
	return http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.ApiPort), n)
}

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("CLASS_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "api",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' API (%s)...", conf.Service.Name, version)

	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the site service: %v", err)
	}

	classService, _, err = class.NewClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot start the class service: %v", err)
	}

	staffService, _, err = staff.NewClient(conf.StaffService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the staff service: %v", err)
	}

	clientService, _, err = account.NewClientClient(conf.ClientService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the client service: %v", err)
	}

	accountService, _, err = account.NewAccountClient(conf.ClientService.Addr)
	if err != nil {
		logger.Fatalf("Cannot start the account service: %v", err)
	}

	// Initialize the sync service too.
	classMBClient := classMb.NewClass_x0020_ServiceSoap("", false, nil)
	saleMBClient := saleMb.NewSale_x0020_ServiceSoap("", false, nil)

	// connect as a producer to the queue service
	logger.Infof("Connecting to the queue service at %s", conf.Queue.Addr)
	cfg := nsq.NewConfig()
	queue, err := nsq.NewProducer(conf.Queue.Addr, cfg)
	if err != nil {
		logger.Fatalf("Cannot start the queue Producer: %v", err)
	}
	queue.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// initialize the ES service
	logger.Infof("Connecting to the ES service at %s", conf.Elasticsearch.Addr)
	elasticsearchService, err := elasticsearch.NewService(&conf.Elasticsearch, logging.CloneLogrusEntry(logger))
	if err != nil {
		logger.Fatalf("Cannot initialize the Elasticsearch service: %v", err)
	}

	syncService = class.NewSyncService(logger, classMBClient, saleMBClient, queue, elasticsearchService, classService, siteService, staffService, &conf.Mindbody)

	if err := run(); err != nil {
		logger.Fatal(err)
	}
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	localLogger := GetRequestLogger(req, logger)

	_, err := classService.Ping(context.Background(), &empty.Empty{})
	if err != nil {
		localLogger.Errorf("Ping error: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))
	return
}
