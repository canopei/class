package main

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/canopei/class"
	"bitbucket.org/canopei/golibs/slices"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"

	"strings"

	accountPb "bitbucket.org/canopei/account/protobuf"
	"bitbucket.org/canopei/class/config"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/mindbody"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
)

// ClassServer is the implementation of a Class server
type ClassServer struct {
	Logger         *logrus.Entry
	SiteService    sitePb.SiteServiceClient
	ClassService   classPb.ClassServiceClient
	StaffService   staffPb.StaffServiceClient
	ClientService  accountPb.ClientServiceClient
	AccountService accountPb.AccountServiceClient
	SyncService    *class.SyncService
	MindbodyConfig *config.MindbodyConfig

	ClassMBClient *classMb.Class_x0020_ServiceSoap
}

// GetClassesByLocationUUIDRequest holds the request
type GetClassesByLocationUUIDRequest struct {
	From string `schema:"from"`
	To   string `schema:"to"`
}

// GetClassesByLocationUUIDResponse holds the response
type GetClassesByLocationUUIDResponse struct {
	Location *sitePb.Location `json:"location"`
	Classes  []*classPb.Class `json:"classes"`
	Staff    []*staffPb.Staff `json:"staff"`
}

// GetClassResponse holds the response
type GetClassResponse struct {
	Location *sitePb.Location `json:"location"`
	Class    *classPb.Class   `json:"class"`
	Staff    *staffPb.Staff   `json:"staff"`
}

// GetClassesResponse holds the response
type GetClassesResponse struct {
	Classes []*GetClassResponse `json:"classes"`
}

// AddAccountToClassRequest holds a request
type AddAccountToClassRequest struct {
	RequirePayment  bool  `json:"requirePayment"`
	ClientServiceID int32 `json:"clientServiceId"`
}

// RemoveAccountFromClassRequest holds a request
type RemoveAccountFromClassRequest struct {
	LateCancel bool `json:"lateCancel"`
}

// NewClassServer creates an instance of an ClassServer
func NewClassServer(
	logger *logrus.Entry,
	siteService sitePb.SiteServiceClient,
	classService classPb.ClassServiceClient,
	staffService staffPb.StaffServiceClient,
	clientService accountPb.ClientServiceClient,
	accountService accountPb.AccountServiceClient,
	syncService *class.SyncService,
	mindbodyConfig *config.MindbodyConfig,
) *ClassServer {
	return &ClassServer{
		Logger:         logger,
		SiteService:    siteService,
		ClassService:   classService,
		StaffService:   staffService,
		ClientService:  clientService,
		AccountService: accountService,
		SyncService:    syncService,
		MindbodyConfig: mindbodyConfig,

		ClassMBClient: classMb.NewClass_x0020_ServiceSoap("", false, nil),
	}
}

// HandleGetClassesByLocationUUID handles a HTTP request for listing classes by location UUID
func (s *ClassServer) HandleGetClassesByLocationUUID(res http.ResponseWriter, req *http.Request) {
	var request GetClassesByLocationUUIDRequest
	schema.NewDecoder().Decode(&request, req.URL.Query())

	vars := mux.Vars(req)
	locationUUID := vars["location_uuid"]

	logger := GetRequestLogger(req, s.Logger)

	logger.Infof("HandleGetClassByLocationUUID %s: %v %v", locationUUID, request, vars)

	ctx := context.Background()

	// Fetch the location from the service
	dbLocations, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Uuids: locationUUID})
	if err != nil {
		logger.Errorf("Error while fetching the location from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(dbLocations.Locations) == 0 {
		logger.Errorf("Location UUID not found in DB.")
		res.WriteHeader(http.StatusNotFound)
		return
	}
	dbLocation := dbLocations.Locations[0]

	dbClasses, err := s.ClassService.GetClassesByLocationID(ctx, &classPb.GetClassesByLocationIDRequest{
		LocationId: dbLocation.Id,
		To:         request.To,
		From:       request.From,
	})
	if err != nil {
		errorCode := grpc.Code(err)
		if errorCode == codes.InvalidArgument {
			logger.Errorf("Invalid arguments for the service: %v", err)
			res.WriteHeader(http.StatusBadRequest)
		} else {
			logger.Errorf("Error while fetching the classes from DB.")
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Fill in the staff - should batch this!
	staffList := []*staffPb.Staff{}
	staffMap := map[int32]bool{}
	for _, dbClass := range dbClasses.Classes {
		if _, ok := staffMap[dbClass.StaffId]; !ok {
			response, err := s.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{
				Ids: []int32{dbClass.StaffId},
			})
			if err != nil {
				logger.Errorf("Error while fetching staff ID %d from DB: %v", dbClass.StaffId, err)
				res.WriteHeader(http.StatusInternalServerError)
				return
			}
			if len(response.Staff) == 0 {
				logger.Errorf("Staff ID %d not found.", dbClass.StaffId)
				continue
			}
			dbStaff := response.Staff[0]

			staffMap[dbClass.StaffId] = true
			staffList = append(staffList, dbStaff)
		}
	}

	response := &GetClassesByLocationUUIDResponse{
		Location: dbLocation,
		Classes:  dbClasses.Classes,
		Staff:    staffList,
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

type omit *struct{}

// HandleGetClasses handles a HTTP request for listing classes by class UUIDs
func (s *ClassServer) HandleGetClasses(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	reqClassUUIDs := vars["class_uuids"]

	logger := GetRequestLogger(req, s.Logger)

	logger.Infof("HandleGetClasses %s", reqClassUUIDs)

	ctx := context.Background()

	response := &GetClassesResponse{}

	// Get the class
	classesResponse, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: reqClassUUIDs})
	if err != nil {
		logger.Errorf("Error while fetching the classes from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, dbClass := range classesResponse.Classes {
		// Get the location
		logger.Debugf("Fetching location ID %d.", dbClass.LocationId)
		dbLocations, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: []int32{dbClass.LocationId}})
		if err != nil {
			logger.Errorf("Error while fetching the location from DB: %v", err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(dbLocations.Locations) == 0 {
			logger.Errorf("Not found location ID %d in DB for class UUID %s.", dbClass.LocationId, dbClass.Uuid)
			continue
		}
		dbLocation := dbLocations.Locations[0]

		// Get the staff
		staffResponse, err := s.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{Ids: []int32{dbClass.StaffId}})
		if err != nil {
			logger.Errorf("Error while fetching the staff from DB: %v", err)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(staffResponse.Staff) == 0 {
			logger.Errorf("Staff ID %d not found in DB. Skipping class...", dbClass.StaffId)
			continue
		}
		dbStaff := staffResponse.Staff[0]

		response.Classes = append(response.Classes, &GetClassResponse{
			Class:    dbClass,
			Location: dbLocation,
			Staff:    dbStaff,
		})
	}

	s.writeJSONResponse(res, response)
	res.WriteHeader(http.StatusOK)
}

// HandleAddAccountToClass adds the given account to the given class
func (s *ClassServer) HandleAddAccountToClass(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	classUUID := vars["class_uuid"]

	decoder := json.NewDecoder(req.Body)
	defer req.Body.Close()
	var request AddAccountToClassRequest
	err := decoder.Decode(&request)
	if err != nil {
		logger.Errorf("Failed while parsing the request body: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	logger.Infof("HandleAddAccountToClass %v %v", vars, request)

	// Validations
	if accountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if classUUID == "" {
		logger.Error("Empty class UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := context.Background()

	// Fetch the account
	accountsClientsResponse, err := s.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{AccountUuids: accountUUID})
	if err != nil {
		logger.Errorf("Error while fetching the clients from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	dbClients, ok := accountsClientsResponse.AccountClients[accountUUID]
	if !ok {
		logger.Errorf("Account UUID '%s' has no clients.", accountUUID)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	// Fetch one class
	response, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: classUUID})
	if err != nil {
		logger.Errorf("Error while fetching the class from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(response.Classes) == 0 {
		logger.Errorf("Class UUID '%s' not found in DB.", classUUID)
		res.WriteHeader(http.StatusNotFound)
		return
	}
	dbClass := response.Classes[0]

	// Fetch the location
	dbLocations, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: []int32{dbClass.LocationId}})
	if err != nil {
		logger.Errorf("Error while fetching the location from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(dbLocations.Locations) == 0 {
		logger.Errorf("Location ID %d not found in DB.", dbClass.LocationId)
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	dbLocation := dbLocations.Locations[0]

	// Check if the account has a client on the class' site
	var client *accountPb.Client
	for _, dbClient := range dbClients.Clients {
		if dbClient.SiteId == dbLocation.SiteId {
			client = dbClient
		}
	}
	if client == nil {
		logger.Errorf("Account UUID '%s' has no client for site ID %d.", accountUUID, dbClass.Location.SiteId)
		res.WriteHeader(http.StatusNotFound)
		return
	}

	// Get the Site from DB
	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Id: client.SiteId})
	if err != nil {
		logger.Errorf("Error or not found while fetching the site from DB: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// MB user creds
	credsUsername := "_" + s.MindbodyConfig.SourceName
	credsPassword := s.MindbodyConfig.SourcePassword
	// This is an exception ONLY for the Sandbox Site, where we cannot authorize our master API creds
	if dbSite.MbId == -99 {
		credsUsername = "Siteowner"
		credsPassword = "apitest1234"
	}

	// Send the request to MB
	mbRequest := mbHelpers.CreateClassMBRequest([]int32{dbSite.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)
	mbRequest.UserCredentials = &classMb.UserCredentials{Username: credsUsername, Password: credsPassword, SiteIDs: mbRequest.SourceCredentials.SiteIDs}

	addClientsRequest := &classMb.AddClientsToClassesRequest{
		MBRequest:      mbRequest,
		ClassIDs:       &classMb.ArrayOfInt{Int: []*mindbody.CustomInt32{&mindbody.CustomInt32{Int: dbClass.MbId}}},
		ClientIDs:      &classMb.ArrayOfString{String: []string{strconv.FormatInt(client.MbId, 10)}},
		RequirePayment: request.RequirePayment,
	}
	if request.ClientServiceID > 0 {
		addClientsRequest.ClientServiceID = &mindbody.CustomInt32{Int: request.ClientServiceID}
	}

	mbResponse, err := s.ClassMBClient.AddClientsToClasses(&classMb.AddClientsToClasses{Request: addClientsRequest})
	mbResult := mbResponse.AddClientsToClassesResult
	if mbResult.ErrorCode.Int != 200 {
		if mbResult.ErrorCode.Int == 201 {
			message := mbResult.Message

			// Here we could just get the single class and the single client; but this way we also check
			// that they acutally exist in the response.
			for _, responseClass := range mbResult.Classes.Class {
				for _, responseClient := range responseClass.Clients.Client {
					switch responseClient.ErrorCode {
					case "601":
						logger.Errorf("Account '%s' (client ID %d) has no available payments for class '%s'.", accountUUID, client.Id, classUUID)
						res.WriteHeader(http.StatusPaymentRequired)
						return
					case "603":
						logger.Errorf("Account '%s' (client ID %d) is already booked at this time.", accountUUID, client.Id)
						res.WriteHeader(http.StatusConflict)
						return
					case "200":
						// Do nothing - ignore 200 error codes clients - they are fine
					default:
						message += strings.Join(responseClient.Messages.String, "; ")
					}
				}
			}

			logger.Errorf("Mindbody API minor response error: %s", message)
			res.WriteHeader(http.StatusBadRequest)
		} else {
			logger.Errorf("Mindbody API response error: %s", mbResult.Message)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	res.WriteHeader(http.StatusOK)
}

// HandleRemoveAccountFromClass removes the given account from the given class
func (s *ClassServer) HandleRemoveAccountFromClass(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	classUUID := vars["class_uuid"]

	decoder := json.NewDecoder(req.Body)
	var request RemoveAccountFromClassRequest
	err := decoder.Decode(&request)
	if err != nil {
		logger.Errorf("Failed while parsing the request body: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	defer req.Body.Close()

	logger.Infof("HandleRemoveAccountFromClass %v %v", vars, request)

	// Validations
	if accountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if classUUID == "" {
		logger.Error("Empty class UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := context.Background()

	// Fetch the account
	accountsClientsResponse, err := s.ClientService.GetAccountsClients(ctx, &accountPb.GetAccountsClientsRequest{AccountUuids: accountUUID})
	if err != nil {
		logger.Errorf("Error while fetching the clients from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	dbClients, ok := accountsClientsResponse.AccountClients[accountUUID]
	if !ok {
		logger.Errorf("Account UUID '%s' has no clients.", accountUUID)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	// Fetch one class
	response, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: classUUID})
	if err != nil {
		logger.Errorf("Error while fetching the class from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(response.Classes) == 0 {
		logger.Errorf("Class UUID '%s' not found in DB.", classUUID)
		res.WriteHeader(http.StatusNotFound)
		return
	}
	dbClass := response.Classes[0]

	// Fetch the location
	dbLocations, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: []int32{dbClass.LocationId}})
	if err != nil {
		logger.Errorf("Error while fetching the location from DB.")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(dbLocations.Locations) == 0 {
		logger.Errorf("Location ID %d not found in DB.", dbClass.LocationId)
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	dbLocation := dbLocations.Locations[0]

	// Check if the account has a client on the class' site
	var client *accountPb.Client
	for _, dbClient := range dbClients.Clients {
		if dbClient.SiteId == dbLocation.SiteId {
			client = dbClient
		}
	}
	if client == nil {
		logger.Errorf("Account UUID '%s' has no client for site ID %d.", accountUUID, dbClass.Location.SiteId)
		res.WriteHeader(http.StatusNotFound)
		return
	}

	// Get the Site from DB
	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Id: client.SiteId})
	if err != nil {
		logger.Errorf("Error or not found while fetching the site from DB: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Send the request to MB
	mbRequest := mbHelpers.CreateClassMBRequest([]int32{dbSite.MbId}, s.MindbodyConfig.SourceName, s.MindbodyConfig.SourcePassword)

	mbResponse, err := s.ClassMBClient.RemoveClientsFromClasses(&classMb.RemoveClientsFromClasses{Request: &classMb.RemoveClientsFromClassesRequest{
		MBRequest:  mbRequest,
		ClassIDs:   &classMb.ArrayOfInt{Int: []*mindbody.CustomInt32{&mindbody.CustomInt32{Int: dbClass.MbId}}},
		ClientIDs:  &classMb.ArrayOfString{String: []string{strconv.FormatInt(client.MbId, 10)}},
		LateCancel: request.LateCancel,
	}})
	mbResult := mbResponse.RemoveClientsFromClassesResult
	if mbResult.ErrorCode.Int != 200 {
		if mbResult.ErrorCode.Int == 201 {
			message := mbResult.Message

			// Here we could just get the single class and the single client; but this way we also check
			// that they acutally exist in the response.
			for _, responseClass := range mbResult.Classes.Class {
				for _, responseClient := range responseClass.Clients.Client {
					if responseClient.ErrorCode != "200" {
						message += strings.Join(responseClient.Messages.String, "; ")
					}
				}
			}

			logger.Errorf("Mindbody API minor response error: %s", message)
			res.WriteHeader(http.StatusBadRequest)
		} else {
			logger.Errorf("Mindbody API response error: %s", mbResult.Message)
			res.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	res.WriteHeader(http.StatusOK)
}

// HandleGetMBClasses fetches a list of classes from MB
func (s *ClassServer) HandleGetMBClasses(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	reqClassUUIDs := vars["class_uuids"]
	classUUIDs := []string{}
	for _, classUUID := range strings.Split(reqClassUUIDs, ",") {
		clClassUUID := strings.TrimSpace(classUUID)
		if !slices.Scontains(classUUIDs, clClassUUID) {
			classUUIDs = append(classUUIDs, clClassUUID)
		}
	}

	logger.Infof("HandleGetMBClasses for %s", classUUIDs)

	err := s.SyncService.SyncSiteClasses(classUUIDs)
	if err != nil {
		logger.Errorf("Error while syncing the classes from MB: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	logger.Info("Forwarding to HandleGetClasses...")
	s.HandleGetClasses(res, req)
}

// HandleGetBookingForAccountAndClass gets a Booking for the given account UUID and class UUID
func (s *ClassServer) HandleGetBookingForAccountAndClass(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	classUUID := vars["class_uuid"]

	logger.Infof("HandleGetBookingForAccountAndClass %v %v", vars)

	// Validations
	if accountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if classUUID == "" {
		logger.Error("Empty class UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := context.Background()

	account, err := s.AccountService.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Account UUID '%s' was not found.", accountUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the account '%s': %v", accountUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	classes, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: classUUID})
	if err != nil {
		s.Logger.Errorf("Error while fetching the class '%s': %v", classUUID, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(classes.Classes) == 0 {
		s.Logger.Infof("Class UUID '%s' was not found.", classUUID)
		res.WriteHeader(http.StatusNotFound)
		return
	}
	class := classes.Classes[0]

	booking, err := s.ClassService.GetBookingForAccountAndClass(ctx, &classPb.Booking{
		AccountId: account.Id,
		ClassId:   class.Id,
	})
	if err != nil {
		s.Logger.Errorf("Error while fetching the booking: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, booking)
	res.WriteHeader(http.StatusOK)
}

// CreateBookingRequest represents a HTTP request
type CreateBookingRequest struct {
	AccountMembershipID int32 `json:"accountMembershipId"`
	MembershipServiceID int32 `json:"membershipServiceId"`
	ServiceID           int32 `json:"serviceId"`
}

// HandleCreateBooking creates a new Booking
func (s *ClassServer) HandleCreateBooking(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]
	classUUID := vars["class_uuid"]

	var request CreateBookingRequest
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&request)
	defer req.Body.Close()
	if err != nil {
		logger.Errorf("Cannot parse the request body JSON: %v", err)
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	logger.Infof("HandleCreateBooking %v %v", vars, request)

	// Validations
	if accountUUID == "" {
		logger.Error("Empty account UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if classUUID == "" {
		logger.Error("Empty class UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	if request.AccountMembershipID < 1 && request.ServiceID < 1 {
		logger.Error("One of the account membership ID or the service ID is required.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := context.Background()

	account, err := s.AccountService.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Account UUID '%s' was not found.", accountUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the account '%s': %v", accountUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	classes, err := s.ClassService.GetClasses(ctx, &classPb.GetClassesRequest{Uuids: classUUID})
	if err != nil {
		s.Logger.Errorf("Error while fetching the class '%s': %v", classUUID, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(classes.Classes) == 0 {
		s.Logger.Infof("Class UUID '%s' was not found.", classUUID)
		res.WriteHeader(http.StatusNotFound)
		return
	}
	class := classes.Classes[0]

	booking, err := s.ClassService.CreateBooking(ctx, &classPb.Booking{
		AccountId:           account.Id,
		ClassId:             class.Id,
		SiteId:              class.SiteId,
		LocationId:          class.LocationId,
		AccountMembershipId: request.AccountMembershipID,
		MembershipServiceId: request.MembershipServiceID,
		ServiceId:           request.ServiceID,
	})
	if err != nil {
		s.Logger.Errorf("Error while creating the booking: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, booking)
	res.WriteHeader(http.StatusOK)
}

// HandleGetSitePrograms gets the programs given a Site UUID
func (s *ClassServer) HandleGetSitePrograms(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	siteUUID := vars["site_uuid"]

	logger.Infof("HandleGetSitePrograms %v %v", vars)

	// Validations
	if siteUUID == "" {
		logger.Error("Empty site UUID provided.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	ctx := context.Background()

	site, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			s.Logger.Infof("Site UUID '%s' was not found.", siteUUID)
			res.WriteHeader(http.StatusNotFound)
		} else {
			s.Logger.Errorf("Error while fetching the site '%s': %v", siteUUID, err)
			res.WriteHeader(http.StatusInternalServerError)
		}

		return
	}

	programs, err := s.ClassService.GetProgramsBySiteID(ctx, &classPb.GetProgramsBySiteIDRequest{SiteId: site.Id})
	if err != nil {
		s.Logger.Errorf("Error while fetching the programs for site ID '%d': %v", site.Id, err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	s.writeJSONResponse(res, programs)
	res.WriteHeader(http.StatusOK)
}

// RegisterRoutes registers the routes to a mux router
func (s *ClassServer) RegisterRoutes(r *mux.Router) {
	r.HandleFunc("/v1/classes/{class_uuids}", s.HandleGetClasses).Methods("GET")
	r.HandleFunc("/v1/classes/mb/{class_uuids}", s.HandleGetMBClasses).Methods("GET")
	r.HandleFunc("/v1/locations/{location_uuid}/classes", s.HandleGetClassesByLocationUUID).Methods("GET")
	r.HandleFunc("/v1/classes/{class_uuid}/account/{account_uuid}", s.HandleAddAccountToClass).Methods("POST")
	r.HandleFunc("/v1/classes/{class_uuid}/account/{account_uuid}", s.HandleRemoveAccountFromClass).Methods("DELETE")
	r.HandleFunc("/v1/classes/{class_uuid}/account/{account_uuid}/bookings", s.HandleGetBookingForAccountAndClass).Methods("GET")
	r.HandleFunc("/v1/classes/{class_uuid}/account/{account_uuid}/bookings", s.HandleCreateBooking).Methods("POST")
	r.HandleFunc("/v1/sites/{site_uuid}/programs", s.HandleGetSitePrograms).Methods("GET")
}

func (s *ClassServer) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}
