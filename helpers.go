package class

import (
	"context"
	"fmt"
	"regexp"
	"time"

	"github.com/Sirupsen/logrus"

	elastic "gopkg.in/olivere/elastic.v5"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	classMb "bitbucket.org/canopei/mindbody/services/class"
)

const sqlDatetimeFormat = "2006-01-02 15:04:05"

func mbClassScheduleToPbClassSchedule(mbClassSchedule *classMb.ClassSchedule) *classPb.ClassSchedule {
	return &classPb.ClassSchedule{
		MbId:                       mbClassSchedule.ID.Int,
		DaySunday:                  mbClassSchedule.DaySunday,
		DayMonday:                  mbClassSchedule.DayMonday,
		DayTuesday:                 mbClassSchedule.DayTuesday,
		DayWednesday:               mbClassSchedule.DayWednesday,
		DayThursday:                mbClassSchedule.DayThursday,
		DayFriday:                  mbClassSchedule.DayFriday,
		DaySaturday:                mbClassSchedule.DaySaturday,
		AllowOpenEnrollment:        mbClassSchedule.AllowOpenEnrollment,
		AllowDateForwardEnrollment: mbClassSchedule.AllowDateForwardEnrollment,
		StartDate:                  mbClassSchedule.StartDate.Time.Format("2006-01-02"),
		EndDate:                    mbClassSchedule.EndDate.Time.Format("2006-01-02"),
		StartTime:                  mbClassSchedule.StartTime.Time.Format("15:04:05"),
		EndTime:                    mbClassSchedule.EndTime.Time.Format("15:04:05"),
	}
}

func hasClassScheduleChanged(dbClassSchedule *classPb.ClassSchedule, mbClassSchedule *classPb.ClassSchedule) bool {
	return dbClassSchedule.DaySunday != mbClassSchedule.DaySunday ||
		dbClassSchedule.DayMonday != mbClassSchedule.DayMonday ||
		dbClassSchedule.DayTuesday != mbClassSchedule.DayTuesday ||
		dbClassSchedule.DayWednesday != mbClassSchedule.DayWednesday ||
		dbClassSchedule.DayThursday != mbClassSchedule.DayThursday ||
		dbClassSchedule.DayFriday != mbClassSchedule.DayFriday ||
		dbClassSchedule.DaySaturday != mbClassSchedule.DaySaturday ||
		dbClassSchedule.StartDate != mbClassSchedule.StartDate ||
		dbClassSchedule.EndDate != mbClassSchedule.EndDate ||
		dbClassSchedule.StartTime != mbClassSchedule.StartTime ||
		dbClassSchedule.EndTime != mbClassSchedule.EndTime ||
		dbClassSchedule.ClassDescriptionId != mbClassSchedule.ClassDescriptionId ||
		dbClassSchedule.StaffId != mbClassSchedule.StaffId ||
		dbClassSchedule.AllowOpenEnrollment != mbClassSchedule.AllowOpenEnrollment ||
		dbClassSchedule.AllowDateForwardEnrollment != mbClassSchedule.AllowDateForwardEnrollment
}

func mbClassDescriptionToPbClassDescription(mbClassDescription *classMb.ClassDescription) *classPb.ClassDescription {
	// Strip the ever-changing imageversion from the Image URL
	re, _ := regexp.Compile("imageversion=[0-9]*")
	mbClassDescription.ImageURL = string(re.ReplaceAll([]byte(mbClassDescription.ImageURL), []byte("")))

	return &classPb.ClassDescription{
		MbId:        mbClassDescription.ID.Int,
		Name:        mbClassDescription.Name,
		Description: mbClassDescription.Description,
		Prereq:      mbClassDescription.Prereq,
		Notes:       mbClassDescription.Notes,
		ImageUrl:    mbClassDescription.ImageURL,
		Active:      mbClassDescription.Active,
	}
}

func hasClassDescriptionChanged(dbClassDescription *classPb.ClassDescription, mbClassDescription *classPb.ClassDescription) bool {
	return dbClassDescription.Name != mbClassDescription.Name ||
		dbClassDescription.Description != mbClassDescription.Description ||
		dbClassDescription.Prereq != mbClassDescription.Prereq ||
		dbClassDescription.Notes != mbClassDescription.Notes ||
		dbClassDescription.ImageUrl != mbClassDescription.ImageUrl ||
		dbClassDescription.Active != mbClassDescription.Active ||
		dbClassDescription.LevelId != mbClassDescription.LevelId
}

func mbClassLevelToPbClassLevel(mbClassLevel *classMb.Level) *classPb.ClassLevel {
	return &classPb.ClassLevel{
		MbId:        mbClassLevel.ID.Int,
		Name:        mbClassLevel.Name,
		Description: mbClassLevel.Description,
	}
}

func hasClassLevelChanged(dbClassLevel *classPb.ClassLevel, mbClassLevel *classPb.ClassLevel) bool {
	return dbClassLevel.Name != mbClassLevel.Name ||
		dbClassLevel.Description != mbClassLevel.Description
}

func mbSessionTypeToPbSessionType(mbSessionType *classMb.SessionType) *classPb.SessionType {
	return &classPb.SessionType{
		MbId:              mbSessionType.ID.Int,
		Name:              mbSessionType.Name,
		DefaultTimeLength: mbSessionType.DefaultTimeLength.Int,
		NumDeducted:       mbSessionType.NumDeducted.Int,
	}
}

func hasSessionTypeChanged(dbSessionType *classPb.SessionType, mbSessionType *classPb.SessionType) bool {
	return dbSessionType.Name != mbSessionType.Name ||
		dbSessionType.DefaultTimeLength != mbSessionType.DefaultTimeLength ||
		dbSessionType.NumDeducted != mbSessionType.NumDeducted
}

func mbProgramToPbProgram(mbProgram *classMb.Program) (*classPb.Program, error) {
	scheduleType, ok := classPb.ScheduleType_value[string(*mbProgram.ScheduleType)]
	if !ok {
		return nil, fmt.Errorf("Unable to convert MB program to PB program - invalid schedule type: %v", *mbProgram.ScheduleType)
	}

	return &classPb.Program{
		MbId:         mbProgram.ID.Int,
		Name:         mbProgram.Name,
		ScheduleType: classPb.ScheduleType(scheduleType),
		CancelOffset: mbProgram.CancelOffset.Int,
	}, nil
}

func hasProgramChanged(dbProgram *classPb.Program, mbProgram *classPb.Program) bool {
	return dbProgram.Name != mbProgram.Name ||
		dbProgram.ScheduleType != mbProgram.ScheduleType ||
		dbProgram.CancelOffset != mbProgram.CancelOffset
}

func mbClassToPbClass(mbClass *classMb.Class) *classPb.Class {
	return &classPb.Class{
		MbId:                mbClass.ID.Int,
		MaxCapacity:         mbClass.MaxCapacity.Int,
		WebCapacity:         mbClass.WebCapacity.Int,
		TotalBooked:         mbClass.TotalBooked.Int,
		TotalBookedWaitlist: mbClass.TotalBookedWaitlist.Int,
		WebBooked:           mbClass.WebBooked.Int,
		IsCanceled:          mbClass.IsCanceled,
		Substitute:          mbClass.Substitute,
		Active:              mbClass.Active,
		IsAvailable:         mbClass.IsAvailable,
		IsWaitlistAvailable: mbClass.IsWaitlistAvailable,
		IsEnrolled:          mbClass.IsEnrolled,
		HideCancel:          mbClass.HideCancel,
		StartDatetime:       mbClass.StartDateTime.Time.Format("2006-01-02 15:04:05"),
		EndDatetime:         mbClass.EndDateTime.Format("2006-01-02 15:04:05"),
	}
}

func hasClassChanged(dbClass *classPb.Class, mbClass *classPb.Class) bool {
	return dbClass.MaxCapacity != mbClass.MaxCapacity ||
		dbClass.ClassScheduleId != mbClass.ClassScheduleId ||
		dbClass.StaffId != mbClass.StaffId ||
		dbClass.WebCapacity != mbClass.WebCapacity ||
		dbClass.TotalBooked != mbClass.TotalBooked ||
		dbClass.TotalBookedWaitlist != mbClass.TotalBookedWaitlist ||
		dbClass.WebBooked != mbClass.WebBooked ||
		dbClass.IsCanceled != mbClass.IsCanceled ||
		dbClass.Substitute != mbClass.Substitute ||
		dbClass.Active != mbClass.Active ||
		dbClass.IsAvailable != mbClass.IsAvailable ||
		dbClass.IsWaitlistAvailable != mbClass.IsWaitlistAvailable ||
		dbClass.IsEnrolled != mbClass.IsEnrolled ||
		dbClass.HideCancel != mbClass.HideCancel ||
		dbClass.StartDatetime != mbClass.StartDatetime ||
		dbClass.EndDatetime != mbClass.EndDatetime
}

func indexClassToES(esService *elasticsearch.Service, indexName string, parentLocationUUID string, dbClass *classPb.Class) (*elastic.IndexResponse, error) {
	return esService.Client.
		Index().
		Index(indexName).
		Type("class").
		Parent(parentLocationUUID).
		Id(dbClass.Uuid).
		BodyJson(GetESClass(dbClass)).
		Do(context.Background())
}

func deleteClassFromES(esService *elasticsearch.Service, indexName string, parentLocationUUID string, dbClassUUID string) (*elastic.DeleteResponse, error) {
	return esService.Client.
		Delete().
		Index(indexName).
		Type("class").
		Parent(parentLocationUUID).
		Id(dbClassUUID).
		Do(context.Background())
}

func timeTrack(logger *logrus.Entry, start time.Time, name string) {
	elapsed := time.Since(start)
	logger.Debugf("%s took %s", name, elapsed)
}
