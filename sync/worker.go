package main

import (
	"encoding/json"
	"time"

	"bitbucket.org/canopei/class"
	"bitbucket.org/canopei/class/config"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// Worker is a queue consumer worker implementation
type Worker struct {
	ID                   int
	Config               *config.SyncConfig
	Logger               *logrus.Entry
	ClassMBClient        *classMb.Class_x0020_ServiceSoap
	SaleMBClient         *saleMb.Sale_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	SyncService          *class.SyncService
	ClassService         classPb.ClassServiceClient
	SiteService          sitePb.SiteServiceClient
	StaffService         staffPb.StaffServiceClient

	Buffers map[string]chan ChanEntry
}

// ChanEntry represents a channel entry containing the original NSQ message and a sync message
type ChanEntry struct {
	NSQMessage  *nsq.Message
	SyncMessage *queueHelper.SyncMessage
}

// NewWorker creates a new Worker instance
func NewWorker(
	id int,
	config *config.Config,
	logger *logrus.Entry,
	classMBClient *classMb.Class_x0020_ServiceSoap,
	saleMBClient *saleMb.Sale_x0020_ServiceSoap,
	classService classPb.ClassServiceClient,
	siteService sitePb.SiteServiceClient,
	staffService staffPb.StaffServiceClient,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
) *Worker {
	workerLogger := logging.CloneLogrusEntry(logger)
	workerLogger.Data["workerID"] = id

	// Get a new SyncService
	syncService := class.NewSyncService(workerLogger, classMBClient, saleMBClient, queue, elasticsearchService, classService, siteService, staffService, &config.Mindbody)

	return &Worker{
		ID:                   id,
		Config:               &config.Sync,
		Logger:               workerLogger,
		ClassMBClient:        classMBClient,
		SaleMBClient:         saleMBClient,
		ClassService:         classService,
		SiteService:          siteService,
		StaffService:         staffService,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		SyncService:          syncService,
	}
}

// HandleMessage handles any queue message
func (w *Worker) HandleMessage(message *nsq.Message) error {
	// Do not auto-finish or requeue messages
	message.DisableAutoResponse()

	// If we cannot unmarshal it into a generic queue message, then drop it
	syncMessage := &queueHelper.SyncMessage{}
	err := json.Unmarshal(message.Body, syncMessage)
	if err != nil {
		w.Logger.Warningf("Unable to unmarshal message: %v", message)
		message.Finish()
		return nil
	}

	// Push the message to the according channel
	switch syncMessage.ObjectType {
	case queueHelper.ObjectTypeSiteClassSchedules:
		w.Buffers[string(syncMessage.ObjectType)] <- ChanEntry{NSQMessage: message, SyncMessage: syncMessage}
	default:
		// If we don't recognize the type, warning and drop the message.
		w.Logger.Warningf("Skipping message with type '%s'.", syncMessage.ObjectType)
		message.Finish()
	}

	return nil
}

// Start makes a worker process the channels
func (w *Worker) Start() {
	w.Buffers = map[string]chan ChanEntry{}

	channels := map[string]func([]ChanEntry){
		string(queueHelper.ObjectTypeSiteClassSchedules): w.ProcessSiteClassSchedules,
	}

	for chName, chProcessor := range channels {
		w.startQueueProcessing(chName, chProcessor)
	}
}

func (w *Worker) startQueueProcessing(name string, process func([]ChanEntry)) {
	w.Buffers[name] = make(chan ChanEntry, w.Config.MaxBatchSize*3)

	go func() {
		for {
			select {
			case f := <-w.Buffers[name]:
				process([]ChanEntry{f})
			}
		}
	}()
}

// ProcessSiteClassSchedules processes the class schedules of a Site (we will get one entry at a time)
func (w *Worker) ProcessSiteClassSchedules(entries []ChanEntry) {
	entry := entries[0]
	siteUUID := entry.SyncMessage.Object.ID

	w.Logger.Infof("Processing sync for site class schedules for site UUID '%s'", siteUUID)
	entry.NSQMessage.Finish()

	err := w.SyncService.SyncSiteClassSchedules(siteUUID)
	if err != nil {
		w.Logger.Errorf("Failed to sync class schedules for site '%s'. Requeuing...: %v", siteUUID, err)
		entry.NSQMessage.Requeue(30 * time.Second)
		return
	}
}
