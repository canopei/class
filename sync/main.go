package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"time"

	"bitbucket.org/canopei/class"
	"bitbucket.org/canopei/class/config"
	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/logging"
	classMb "bitbucket.org/canopei/mindbody/services/class"
	saleMb "bitbucket.org/canopei/mindbody/services/sale"
	"bitbucket.org/canopei/site"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"bitbucket.org/canopei/staff"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
	nsq "github.com/nsqio/go-nsq"
)

var (
	conf                 *config.Config
	logger               *logrus.Entry
	classMBClient        *classMb.Class_x0020_ServiceSoap
	saleMBClient         *saleMb.Sale_x0020_ServiceSoap
	queue                *nsq.Producer
	elasticsearchService *elasticsearch.Service
	classService         classPb.ClassServiceClient
	siteService          sitePb.SiteServiceClient
	staffService         staffPb.StaffServiceClient
	version              string
)

func init() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("CLASS_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "sync",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))
}

func main() {
	logger.Infof("Booting '%s' sync (%s)...", conf.Service.Name, version)

	var err error

	// prepare the MB SOAP client
	classMBClient = classMb.NewClass_x0020_ServiceSoap("", false, nil)
	saleMBClient = saleMb.NewSale_x0020_ServiceSoap("", false, nil)

	// connect to the Class service
	classService, _, err = class.NewClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot connect to the class service: %v", err)
	}

	// connect to the Site service
	siteService, _, err = site.NewClient(conf.SiteService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the site service: %v", err)
	}

	// connect to the Staff service
	staffService, _, err = staff.NewClient(conf.StaffService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the staff service: %v", err)
	}

	// connect as a producer to the queue service
	logger.Infof("Connecting to the queue service at %s", conf.Queue.Addr)
	cfg := nsq.NewConfig()
	queue, err = nsq.NewProducer(conf.Queue.Addr, cfg)
	if err != nil {
		logger.Fatalf("Cannot start the queue Producer: %v", err)
	}
	queue.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// initialize the ES service
	logger.Infof("Connecting to the ES service at %s", conf.Elasticsearch.Addr)
	elasticsearchService, err = elasticsearch.NewService(&conf.Elasticsearch, logging.CloneLogrusEntry(logger))
	if err != nil {
		logger.Fatalf("Cannot initialize the Elasticsearch service: %v", err)
	}

	// Configure the NSQ consumer
	consumerConfig := nsq.NewConfig()
	consumerConfig.MaxInFlight = conf.Sync.MaxInFlight
	consumerConfig.LookupdPollInterval = time.Duration(conf.Sync.ReconnectInterval) * time.Second

	q, _ := nsq.NewConsumer(conf.Queue.SyncTopic, "sync_class", consumerConfig)
	q.SetLogger(logging.NewNSQLogrusLogger(logger), nsq.LogLevelInfo)

	// Create and start the workers
	logger.Infof("Starting workers on %s", conf.Queue.Addr)
	for i := 1; i <= conf.Sync.Workers; i++ {
		worker := CreateWorker(i)
		worker.Start()
		q.AddHandler(nsq.HandlerFunc(worker.HandleMessage))
	}

	err = q.ConnectToNSQD(conf.Queue.Addr)
	if err != nil {
		logger.Fatal("Could not connect to NSQd")
	}

	logger.Info("Workers started.")

	// Gracefully handle SIGINT and SIGTERM
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case <-q.StopChan:
			return
		case <-sigChan:
			q.Stop()
		}
	}
}

// CreateWorker creates a new Worker instance
func CreateWorker(id int) *Worker {
	return NewWorker(id, conf, logger, classMBClient, saleMBClient, classService, siteService, staffService, queue, elasticsearchService)
}
